# UNIGIS Master Thesis Christoph Schaller

---

This is a repository for storing files connected to the UNIGIS Masterthesis of Christoph Schaller

Folder layput:
* [thesis] - the actual thesis and connected documents
* [project] - general project related documents
* [documentation] - additional documentation
* [src] - source files and project files (QGIS, Rhino/Grasshopper)
