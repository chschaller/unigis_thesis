##Load Libraries

library(DBI)
library(RODBC)

library(ggplot2)
library(reshape)
library(plyr)

library(grid)
library(gridExtra)

library(readr)
library(tidyr)
library(ggplot2)
library(ggridges)
library(Hmisc)
library(plyr)
library(RColorBrewer)
library(reshape2)

source("https://gist.githubusercontent.com/benmarwick/2a1bb0133ff568cbe28d/raw/fb53bd97121f7f9ce947837ef1a4c65a73bffb3f/geom_flat_violin.R")



#all_content = readLines("c:/school/UniGIS/Master_Thesis/git/src/sql/results/Analysis_Sensitivität_Detail.csv")
all_content = readLines("c:/temp/Analysis_Sensitivität_Detail.csv")
skip_second = all_content[-2]
df = read.csv(textConnection(skip_second), header = TRUE, sep = "|")

all_content <- NULL

str(df)


##Establish DB Connection
#Local ODBC DSN of the specified name needs to be configured in system level!
ch <- odbcConnect("pg_localhost_dencity")

qerySensitivityDetail <- "SELECT sid, titel, pid, bmz, gfz, uz, iuz, igfz, ibmz, grosser_grenzabstand, kleiner_grenzabstand, geschosszahl, nutzungsziffer, ausnuetzung_art, ausnuetzung_art_label, gruenflaechenziffer, gesamthoehe, gruppierung
	FROM public.sensitivitaet_detail"
#Execute query
df <- sqlQuery(ch, qerySensitivityDetail)



df$ausnuetzung <- factor(df$ausnuetzung_art, levels=c("1","2","3","0"), labels=c("GFZ", "BMZ", "ÜZ","Keine"))  
df$projekt <- factor(df$pid, levels=c("4","3"), labels=c("Langenthal","Uettligen")) 
df$gruppiert <- factor(df$gruppierung, levels=c("1","0"), labels=c("Ja","Nein")) 

df$nzg <- NA
df$nzg[df$ausnuetzung_art==1] <- df$gfz[df$ausnuetzung_art==1] 
df$nzg[df$ausnuetzung_art==2] <- df$bmz[df$ausnuetzung_art==2]
df$nzg[df$ausnuetzung_art==3] <- df$uz[df$ausnuetzung_art==3]


df$nzi <- NA
df$nzi[df$ausnuetzung_art==1] <- df$igfz[df$ausnuetzung_art==1] 
df$nzi[df$ausnuetzung_art==2] <- df$ibmz[df$ausnuetzung_art==2]
df$nzi[df$ausnuetzung_art==3] <- df$iuz[df$ausnuetzung_art==3]


str(df)


dfgfz <- df
dfgfz$Plot <- "Geschossflächenziffer"
dfgfz$data <- dfgfz$igfz

dfbmz <- df
dfbmz$Plot <- "Baumasseziffer"
dfbmz$data <- dfbmz$ibmz

dfuz <- df
dfuz$Plot <- "Überbauungsziffer"
dfuz$data <- dfuz$iuz

ds <- rbind(dfgfz, dfbmz, dfuz)

str(ds)

##Source: https://github.com/tidyverse/ggplot2/wiki/Share-a-legend-between-two-ggplot2-graphs
grid_arrange_shared_legend <- function(..., ncol = length(list(...)), nrow = 1, position = c("bottom", "right")) {
  plots <- list(...)
  position <- match.arg(position)
  g <- ggplotGrob(plots[[1]] + theme(legend.position = position))$grobs
  legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
  lheight <- sum(legend$height)
  lwidth <- sum(legend$width)
  gl <- lapply(plots, function(x) x + theme(legend.position="none"))
  gl <- c(gl, ncol = ncol, nrow = nrow)
  
  combined <- switch(position,
                     "bottom" = arrangeGrob(do.call(arrangeGrob, gl),
                                            legend,
                                            ncol = 1,
                                            heights = unit.c(unit(1, "npc") - lheight, lheight)),
                     "right" = arrangeGrob(do.call(arrangeGrob, gl),
                                           legend,
                                           ncol = 2,
                                           widths = unit.c(unit(1, "npc") - lwidth, lwidth)))
  
  grid.newpage()
  grid.draw(combined)
  
  # return gtable invisibly
  invisible(combined)
  
}


##Source: https://stackoverflow.com/a/22292080
align_plots1 <- function (...) {
  pl <- list(...)
  stopifnot(do.call(all, lapply(pl, inherits, "gg")))
  gl <- lapply(pl, ggplotGrob)
  bind2 <- function(x, y) gtable:::rbind_gtable(x, y, "first")
  combined <- Reduce(bind2, gl[-1], gl[[1]])
  wl <- lapply(gl, "[[", "widths")
  combined$widths <- do.call(grid::unit.pmax, wl)
  grid::grid.newpage()
  grid::grid.draw(combined)
}

## geom_split_violin https://stackoverflow.com/a/45614547
GeomSplitViolin <- ggproto("GeomSplitViolin", GeomViolin, draw_group = function(self, data, ..., draw_quantiles = NULL){
  data <- transform(data, xminv = x - violinwidth * (x - xmin), xmaxv = x + violinwidth * (xmax - x))
  grp <- data[1,'group']
  newdata <- plyr::arrange(transform(data, x = if(grp%%2==1) xminv else xmaxv), if(grp%%2==1) y else -y)
  newdata <- rbind(newdata[1, ], newdata, newdata[nrow(newdata), ], newdata[1, ])
  newdata[c(1,nrow(newdata)-1,nrow(newdata)), 'x'] <- round(newdata[1, 'x']) 
  if (length(draw_quantiles) > 0 & !scales::zero_range(range(data$y))) {
    stopifnot(all(draw_quantiles >= 0), all(draw_quantiles <= 
                                              1))
    quantiles <- ggplot2:::create_quantile_segment_frame(data, draw_quantiles)
    aesthetics <- data[rep(1, nrow(quantiles)), setdiff(names(data), c("x", "y")), drop = FALSE]
    aesthetics$alpha <- rep(1, nrow(quantiles))
    both <- cbind(quantiles, aesthetics)
    quantile_grob <- GeomPath$draw_panel(both, ...)
    ggplot2:::ggname("geom_split_violin", grid::grobTree(GeomPolygon$draw_panel(newdata, ...), quantile_grob))
  }
  else {
    ggplot2:::ggname("geom_split_violin", GeomPolygon$draw_panel(newdata, ...))
  }
})

geom_split_violin <- function (mapping = NULL, data = NULL, stat = "ydensity", position = "identity", ..., draw_quantiles = NULL, trim = TRUE, scale = "area", na.rm = FALSE, show.legend = NA, inherit.aes = TRUE) {
  layer(data = data, mapping = mapping, stat = stat, geom = GeomSplitViolin, position = position, show.legend = show.legend, inherit.aes = inherit.aes, params = list(trim = trim, scale = scale, draw_quantiles = draw_quantiles, na.rm = na.rm, ...))
}
## geom_split_violin end



ggplot(data = ds, aes(y = data, x = as.factor(nutzungsziffer), fill = projekt)) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("")+
  scale_color_brewer(palette = "Dark2") +
  scale_fill_brewer(palette = "Dark2") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom", legend.title = element_blank()) +
  facet_grid(Plot ~ ausnuetzung, scales = "free_y") 


##Gruppierung
grpgfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = gruppiert)) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "Set1") +
  scale_fill_brewer(palette = "Set1") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Gruppierung") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grpuz <-ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = gruppiert)) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "Set1") +
  scale_fill_brewer(palette = "Set1") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Gruppierung") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(grpgfz, grpuz, ncol = 2, nrow = 1, position = "bottom")

##Geschosszahl
gszgfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = as.factor(geschosszahl))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "Set2") +
  scale_fill_brewer(palette = "Set2") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Geschosszahl") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

gszuz <- ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = as.factor(geschosszahl))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "Set2") +
  scale_fill_brewer(palette = "Set2") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Geschosszahl") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(gszgfz, gszuz, ncol = 2, nrow = 1, position = "bottom")

##Grosser Grenzabstand
ggagfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = as.factor(grosser_grenzabstand))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "PuOr") +
  scale_fill_brewer(palette = "PuOr") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Grosser Grenzabstand") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

ggauz <- ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = as.factor(grosser_grenzabstand))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "PuOr") +
  scale_fill_brewer(palette = "PuOr") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Grosser Grenzabstand") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(ggagfz, ggauz, ncol = 2, nrow = 1, position = "bottom")


##Kleiner Grenzabstand
kgagfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = as.factor(kleiner_grenzabstand))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "RdBu") +
  scale_fill_brewer(palette = "RdBu") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Kleiner Grenzabstand") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

kgauz <- ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = as.factor(kleiner_grenzabstand))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "RdBu") +
  scale_fill_brewer(palette = "RdBu") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Kleiner Grenzabstand") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(kgagfz, kgauz, ncol = 2, nrow = 1, position = "bottom")


##Grünflächenziffer
gzgfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 


gzuz <-ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_boxplot(width = .5, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(gzgfz, gzuz, ncol = 2, nrow = 1, position = "bottom")

#Violin
gzgfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_violin(width = 1, trim = FALSE, draw_quantiles = c(0.25, 0.5, 0.75), alpha = 0.5) +
  #geom_boxplot(width = .1, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 


gzuz <-ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_violin(width = 1, trim = FALSE, draw_quantiles = c(0.25, 0.5, 0.75), alpha = 0.5) +
  #geom_boxplot(width = .1, alpha = 0.5, outlier.size = 0.8, outlier.alpha = 0.8) +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(gzgfz, gzuz, ncol = 2, nrow = 1, position = "bottom")


#Split Violin
gzgfz <- ggplot(data = df, aes(y = igfz, x = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_split_violin(width = .95, trim = FALSE, alpha = 0.5, color="darkgrey") +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 


gzuz <-ggplot(data = df, aes(y = iuz, x = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_split_violin(width =  .95, trim = FALSE, alpha = 0.5, color="darkgrey") +
  scale_x_discrete(name = "Nutzungsziffer") + 
  scale_y_continuous() + 
  ylab("Überbauungsziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  #coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(gzgfz, gzuz, ncol = 2, nrow = 1, position = "bottom")

#Ridges Histogram
gzgfz <- ggplot(data = df, aes(x = igfz, y = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_density_ridges(stat = "binline", bins = 35, alpha = 0.5, scale = 0.9, color="darkgrey") +
  scale_x_continuous() + 
  scale_y_discrete() + 
  xlab("Überbauungsziffer")+
  ylab("Nutzungsziffer")+
  ylab("Geschossflächenziffer")+
  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom",  strip.text.y = element_blank()) +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 


gzuz <-ggplot(data = df, aes(x = iuz, y = as.factor(nutzungsziffer), fill = as.factor(gruenflaechenziffer))) +
  geom_density_ridges(stat = "binline", bins = 35, alpha = 0.5, scale = 0.9, color="darkgrey") +
  scale_x_continuous() + 
  scale_y_discrete() + 
  xlab("Überbauungsziffer")+
  ylab("Nutzungsziffer")+  scale_color_brewer(palette = "Greens") +
  scale_fill_brewer(palette = "Greens") +
  coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom") +
  labs(fill="Grünflächenziffer") +
  facet_grid(ausnuetzung ~ projekt, scales = "free_y") 

grid_arrange_shared_legend(gzgfz, gzuz, ncol = 2, nrow = 1, position = "bottom")


sum_gz<- ddply(df,ausnuetzung ~ projekt+nutzungsziffer+gruenflaechenziffer, summarise, 
              min_uz = min(iuz),
              q2_uz = quantile(iuz)[2],
              mean_uz = mean(iuz),
              median_uz = median(iuz), 
              q3_uz = quantile(iuz)[3],
              max_uz = max(iuz),
              sd_uz = sd(iuz),
              var_uz = var(iuz),
              n_uz  = sum(!is.na(iuz)),
              

              min_gfz = min(igfz),
              q2_gfz = quantile(igfz)[2],
              mean_gfz = mean(igfz),
              median_gfz = median(igfz), 
              q3_gfz = quantile(igfz)[3],
              max_gfz = max(igfz),
              sd_gfz = sd(igfz),
              var_gfz = var(igfz),
              n_gfz  = sum(!is.na(igfz)))

sum_gz
format(sum_gz, digits=2)

write.csv(sum_gz, file = "c:/temp/Summary_Grünflächenziffer.csv")
