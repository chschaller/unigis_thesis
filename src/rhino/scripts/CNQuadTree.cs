using Rhino;
using Rhino.Geometry;
using Rhino.DocObjects;
using Rhino.Collections;

using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;

using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;



/// <summary>
/// This class will be instantiated on demand by the Script component.
/// </summary>
public class Script_Instance : GH_ScriptInstance
{
#region Utility functions
  /// <summary>Print a String to the [Out] Parameter of the Script component.</summary>
  /// <param name="text">String to print.</param>
  private void Print(string text) { /* Implementation hidden. */ }
  /// <summary>Print a formatted String to the [Out] Parameter of the Script component.</summary>
  /// <param name="format">String format.</param>
  /// <param name="args">Formatting parameters.</param>
  private void Print(string format, params object[] args) { /* Implementation hidden. */ }
  /// <summary>Print useful information about an object instance to the [Out] Parameter of the Script component. </summary>
  /// <param name="obj">Object instance to parse.</param>
  private void Reflect(object obj) { /* Implementation hidden. */ }
  /// <summary>Print the signatures of all the overloads of a specific method to the [Out] Parameter of the Script component. </summary>
  /// <param name="obj">Object instance to parse.</param>
  private void Reflect(object obj, string method_name) { /* Implementation hidden. */ }
#endregion

#region Members
  /// <summary>Gets the current Rhino document.</summary>
  private readonly RhinoDoc RhinoDocument;
  /// <summary>Gets the Grasshopper document that owns this script.</summary>
  private readonly GH_Document GrasshopperDocument;
  /// <summary>Gets the Grasshopper script component that owns this script.</summary>
  private readonly IGH_Component Component;
  /// <summary>
  /// Gets the current iteration count. The first call to RunScript() is associated with Iteration==0.
  /// Any subsequent call within the same solution will increment the Iteration count.
  /// </summary>
  private readonly int Iteration;
#endregion

  /// <summary>
  /// This procedure contains the user code. Input parameters are provided as regular arguments,
  /// Output parameters as ref arguments. You don't have to assign output parameters,
  /// they will have a default value.
  /// </summary>
  private void RunScript(List<Brep> poly, List<double> ang, List<double> len, List<Line> edg, double lim, int depthLimit, int maxDepth, double targetArea, ref object A, ref object B, ref object C, ref object D, ref object E, ref object dW, ref object dH, ref object P)
  {
    //Return if no sufficient inputs are given
    if (poly == null || poly.Count == 0) {
      A = null;
      B = null;

      return;
    }

    //"upper left"
    Point3d corner = edg[2].To;
    Line lineWidth = edg[2];
    Line lineHeight = edg[1];
    double w = Math.Ceiling(len[0]);
    double h = Math.Ceiling(len[1]);
    double dim = Math.Max(w, h);

    //square subdivision
    w = dim;
    h = dim;

    //Direction vectors from bounding box sides
    Vector3d dirW = lineWidth.Direction;
    Vector3d dirH = lineHeight.Direction;

    dirW.Unitize();
    dirH.Unitize();
    dirW.Reverse();

    //Origin of subdivision square
    P = corner;
    dW = dirW;
    dH = dirH;

    //Construct quad tree
    double tolerance = this.RhinoDocument.ModelAbsoluteTolerance;
    CNQuadTree tree = new CNQuadTree(corner, w, h, dirW, dirH, poly, lim, depthLimit, maxDepth, tolerance);

    //Prepare tree
    double treeArea = 0;
    tree.determineConnectedComponents();
    tree.determineFillRatio();

    //List for collecting geometries
    List<Curve> geoms = new List<Curve>();

    //Test if there are connected components
    if (tree.connectedComponents.Values.Count == 0) {
      //no connected components -> no quads inside input areas
      //=> fall back to scaled input areas
      double polyArea = 0;
      poly.ForEach(pol => polyArea += pol.GetArea());
      var p = ScaleBreps(poly, targetArea, polyArea);
      A = p;
      polyArea = 0;
      p.ForEach(pol => polyArea += pol.GetArea());
      B = polyArea;
      return;
    }

    //Eliminate small freestanding Quads
    if (tree.whiteNodes.Count > 1){
      //Limits depth limit for freestanding quads to be eliminated
      int singleCullDepth = tree.maxDepth;
      List<CNQNode> singles = new List<CNQNode>();

      //Test for all nodes whether they are "freestanding"
      foreach(CNQNode n in tree.whiteNodes.Values) {
        if(n.exposure < 0.3 && n.depth <= singleCullDepth) {
          singles.Add(n);
        }
      }

      //Turn exposed nodes to black
      if (singles.Count > 0){
        foreach(CNQNode n in singles) {
          tree.turnNodeBlack(n);
        }

        //recalculate fill ratio and exposure
        tree.determineFillRatio();
      }
    }

/*
    //Alternate derivation strategy based on collecting Quads from connected components
    if (tree.connectedComponents.Values.Count > 1) {

      double maxArea = tree.connectedComponentAreas.Values.Max();

      List<Brep> brepGeom = new List<Brep>();

      //Iterate over all connected components (i.e. patches of internal Quads)
      foreach(int key in tree.connectedComponents.Keys) {
        //Continue if component consists of at least 2 Quads
        //and its area is at least 25% of the largest component
        //(i.e. ignore small components and single Quads)
        if (tree.connectedComponentAreas.Count > 1 && tree.connectedComponentAreas[key] > 0.25 * maxArea) {
          //Add component Quads to output until target area is reached
          List<Curve> curveGeom = new List<Curve>();
          foreach(CNQNode n in tree.connectedComponents[key]) {
            //Only add Quad if parent quad is at least 30% white
            //(try to avoid unfavorable shapes resulting from partial filling)
            if (n.parent != null && n.parent.filledAreaRatio >= 0.3 && treeArea < targetArea) {
              curveGeom.Add(n.rect);
              treeArea += n.area;
            }
          }
          //Add to actual output
          if (curveGeom.Count > 0){
            brepGeom.AddRange(Brep.JoinBreps(Brep.CreatePlanarBreps(curveGeom), tolerance));
          }
        }
      }

      if (treeArea > targetArea && brepGeom.Count > 0) {
        //Quads were collected and area is larger than target
        //-> scale down Breps
        List<Brep> scaledBrepGeom = new List<Brep>();

        double scaledArea = 0;
        double scaleFactor = targetArea / treeArea;
        foreach(Brep b in brepGeom) {
          scaledBrepGeom.Add(ScaleBrep(b, targetArea, treeArea));
          scaledArea += b.GetArea();
        }
        A = scaledBrepGeom;//Brep.JoinBreps(Brep.CreatePlanarBreps(geoms), tolerance);
        B = scaledArea;
        return;
      }

      if (brepGeom.Count > 0) {
        //Quads were collected and area is smaller than target
        //-> output collected Breps
        A = brepGeom;
        B = treeArea;
      } else {
        //No Quads were collected -> fallback to scaled input areas
        var p = ScaleBrep(poly, targetArea, treeArea);
        A = p;
        B = p.GetArea();
      }
  }
    */


    /*
    //Yet another derivation approach by naive depth first search
    //Ignores target area
    Stack<CNQNode> stack = new Stack<CNQNode>();
    CNQNode cNode = tree.root;
    stack.Push(cNode);
    int depthLimit = 2;
    double cullLimit = 0.5;
    while (!(stack.Count == 0)) {
      cNode = stack.Pop();
      if (cNode.depth > depthLimit
        || cNode.depth == depthLimit && cNode.filledAreaRatio >= cullLimit
      || cNode.depth < depthLimit) { //process or add
        if (cNode.value == -1) { //process
          if (cNode.children[quadrant.nw] != null) {
            stack.Push(cNode.children[quadrant.se]);
            stack.Push(cNode.children[quadrant.sw]);
            stack.Push(cNode.children[quadrant.ne]);
            stack.Push(cNode.children[quadrant.nw]);
          }
        } else if (cNode.value == 1) {
          geoms.Add(cNode.rect);
        }
      }
    }

    A = geoms;
    B = treeArea;
  */

    if (tree.connectedComponents.Values.Count > 0) {
      //there are connected components (i.e. Quads inside the input areas)
      double maxArea = tree.connectedComponentAreas.Values.Max();

      List<Brep> brepGeom = new List<Brep>();

      //If more than one connected component
      //-> eliminate components smaller than 25% of largest component
      //(prefer larger patches for deriving footprints and avoid artifacts/small patches)
      HashSet<int> excludedComponents = new HashSet<int>();
      if (tree.connectedComponentAreas.Count > 1) {
        foreach(int key in tree.connectedComponents.Keys) {
          if (tree.connectedComponentAreas[key] < 0.25 * maxArea){
            excludedComponents.Add(key);
          }
        }
      }
      bool checkExclusion = excludedComponents.Count == 0;

      //Collect Quads using depth first search until target area is reached
      List<Curve> curveGeom = new List<Curve>();

      Stack<CNQNode> stack = new Stack<CNQNode>();
      CNQNode cNode = tree.root;
      stack.Push(cNode);
      //limits for inclusion (prefer medium to large blocks)
      int minDepthLimit = 2;
      int maxDepthLimit = maxDepth - 2;
      //double cullLimit = 0.3;
      while (!(stack.Count == 0)) {
        cNode = stack.Pop();
        if (cNode.value == 0) {
          //leaf oudside input -> no operation
          continue;
        } else if (cNode.value == 1
          && treeArea < targetArea
          && (!checkExclusion ||
          checkExclusion && !excludedComponents.Contains(cNode.connectedComponent))
        ) {
          //leaf inside input and not excluded -> collect Quad
          curveGeom.Add(cNode.rect);
          treeArea += cNode.area;
        } else {
          //internal Quad -> check inclusion
          if (cNode.depth < minDepthLimit
            || cNode.depth > maxDepthLimit
            || treeArea < targetArea
            //&& cNode.filledAreaRatio >= cullLimit
            //&& (cNode.filledAreaRatio == 1 || cNode.borderLength>2.5*cNode.w)
            //&& !(cNode.filledAreaRatio == 0.5 && !(cNode.filledAreaRatio == 0.5 && (cNode.borderLength == 2 * cNode.w || cNode.borderLength == 2.5 * cNode.w))
          ) {
            //inclusin criteria are met -> process children
            if (cNode.children[quadrant.nw] != null) {
              stack.Push(cNode.children[quadrant.se]);
              stack.Push(cNode.children[quadrant.sw]);
              stack.Push(cNode.children[quadrant.ne]);
              stack.Push(cNode.children[quadrant.nw]);
            }
          }
        }
      }

      //Merge individual Quads to connected Breps
      brepGeom.AddRange(Brep.JoinBreps(Brep.CreatePlanarBreps(curveGeom), tolerance));

      //Prepare output
      if (treeArea > targetArea && brepGeom.Count > 0) {
        //Quads were collected and area is larger than target
        //-> scale down Breps
        List<Brep> scaledBrepGeom = new List<Brep>();

        double scaledArea = 0;
        double scaleFactor = targetArea / treeArea;
        foreach(Brep b in brepGeom) {
          scaledBrepGeom.Add(ScaleBrep(b, targetArea, treeArea));
          scaledArea += b.GetArea();
        }
        A = scaledBrepGeom;
        B = scaledArea;
        C = treeArea;
        D = targetArea / treeArea;

      } else if (brepGeom.Count > 0) {
        //Quads were collected and area is smaller than target
        //-> return Breps
        A = brepGeom;
        B = treeArea;
      } else {
        //No Quads were Collected -> fall back to scaled input areas
        var p = ScaleBreps(poly, targetArea, treeArea);
        A = p;
        double polyArea = 0;
        p.ForEach(pol => polyArea += pol.GetArea());
        B = polyArea;
      }

    }
  }

  // <Custom additional code> 

  /// <summary>
  ///Method for scaling down a Brep surface to a target area. No scaling is
  ///performed if area is already smaller.
  /// </summary>
  /// <param name="poly"></param>
  /// <param name="targetArea"></param>
  /// <param name="area"></param>
  /// <returns></returns>
  public Brep ScaleBrep (Brep poly, double targetArea, double area) {
    //Calculate AreaMassProperties to determine Brep area and centroid
    AreaMassProperties amp = AreaMassProperties.Compute(poly);
    if (amp.Area < targetArea){
      //area already smaller than target -> return original Brep
      return poly;
    } else {
      //area larger than target -> scale down Brep
      Point3d center = amp.Centroid;
      double factor = Math.Sqrt(targetArea / area);
      poly.Transform(Transform.Scale(center, factor));
      return poly;
    }
  }

  /// <summary>
  ///Method for scaling down a list of Brep surfaces such that their combined area
  ///is equal or smaller thn a target area. No scaling is performed if ths
  ///area is already smaller.
  /// </summary>
  /// <param name="poly"></param>
  /// <param name="targetArea"></param>
  /// <param name="area"></param>
  /// <returns></returns>
  public List<Brep> ScaleBreps (List<Brep> poly, double targetArea, double area) {
    //Calculate AreaMassProperties of all Breps
    List<AreaMassProperties> amps = new List<AreaMassProperties>();
    for (int i = 0; i < poly.Count; i++){
      amps.Add(AreaMassProperties.Compute(poly[i]));
    }
    //Sum the area
    double polyArea = 0;
    amps.ForEach(amp => polyArea += amp.Area);

    if (polyArea < targetArea){
      //summed area already smaller than target -> return original Breps
      return poly;
    } else {
      //area larger than target -> scale down individual Breps
      double factor = Math.Sqrt(targetArea / area);

      for (int i = 0; i < poly.Count;i++) {
        Point3d center = amps[0].Centroid;
        poly[i].Transform(Transform.Scale(center, factor));
      }
      return poly;
    }
  }

  ///
  ///Cardinal Neighbor Quadtree
  ///

  ///Direction "constants"/indexes
  public class quadrant { public static int nw = 0, ne = 1, sw = 2, se = 3;}
  ///Neighbor "constants"/indexes
  public class neighbor { public static int w = 0, n = 1, e = 2, s = 3;}

  /// <summary>
  ///Node class for Cardinal Neighbor Quad Tree.
  ///
  ///Partially based on "Region quadtrees in Go" by Aurelien Rainone 2017
  ///https://github.com/aurelien-rainone/go-rquad
  /// </summary>
  public class CNQNode {
    //Direction "constants"
    public static int[] opposite = new int[]{neighbor.e, neighbor.s, neighbor.w, neighbor.n};
    public static int[] traversal = new int[]{neighbor.s, neighbor.e, neighbor.n, neighbor.w};

    //Node Type: 0: outside/black leaf, 1: inside/white leaf, -1: internal
    public sbyte value;
    //Covered area
    public double area;
    //Width and Height, may differ in non square case
    public double w;
    public double h;

    //"Lower left" corner
    public Point3d p0;
    //Corner coordinates
    public double x, y;
    //Tree level
    public byte depth;

    //Quad geometry
    public Curve rect;
    //Parent node
    public CNQNode parent;

    //Child nodes
    public CNQNode[] children;
    //Cardinal Neighbors
    public CNQNode[] neighbors;
    //Z-Order ID
    public int quad;

    //Degree of exposure (-1: unset)
    //(i.e. ratio of black border to circumference)
    public double exposure = -1;
    //Lenght of borders to black nodes (-1: unset)
    public double borderLength = -1;
    public double borderLengthW = -1;
    public double borderLengthN = -1;
    public double borderLengthE = -1;
    public double borderLengthS = -1;
    //Filled (white) area (-1: unset)
    public double filledArea = -1;
    //Ratio of filled to total area (-1: unset)
    public double filledAreaRatio = -1;

    //ID of connected component
    public int connectedComponent;

    /// <summary>
    /// Create a new CNQNode
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="quad">z-order quad ID</param>
    /// <param name="value">initial value</param>
    /// <param name="p0">"lower left" corner</param>
    /// <param name="x">x coordinate of corner</param>
    /// <param name="y">y coordinate of corner</param>
    /// <param name="w">numeric width of quad</param>
    /// <param name="h">numeric height of quad</param>
    /// <param name="rect">quad geometry</param>
    public CNQNode(CNQNode parent, int quad, sbyte value, Point3d p0, double x, double y, double w, double h, Curve rect){
      this.value = value;
      this.children = new CNQNode[4];
      this.neighbors = new CNQNode[4];
      this.parent = parent;
      this.quad = quad;

      this.connectedComponent = -1;

      this.p0 = p0;
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.area = this.w * this.h;
      this.rect = rect;
      //determine level
      if (this.parent != null) {
        this.depth = (byte) (this.parent.depth + 1);
      }
      else {
        this.depth = 0;
      }
    }

    /// <summary>
    /// Determine cardinal neighbors of this node
    ///
    /// Compare Steps in
    /// QASEM, S. W. & TOUIR, A. A. 2015.
    /// Cardinal Neighbor Quadtree:
    /// a New Quadtree-based Structure for Constant-Time Neighbor Finding.
    /// International Journal of Computer Applications, 132(8), 22-30.
    /// </summary>
    /// <returns></returns>
    public void updateNeighbors() {
      // On each direction, a full traversal of the neighbors
      // should be performed.  In every quadrant where a reference
      // to the parent quadrant is stored as the Cardinal Neighbor,
      // it should be replaced by one of its children created after
      // the decomposition

      //Step 2.1: Updating Cardinal Neighbors of SW sub-Quadrant.
      if (!(this.parent == null || this.neighbors[neighbor.w] == null)) {
        // Quadrant not on west border
        if (this.neighbors[neighbor.n] != null) {
          if (this.neighbors[neighbor.n].w < this.w) {
            CNQNode c0 = this.children[quadrant.nw];
            c0.neighbors[neighbor.n] = this.neighbors[neighbor.n];
            // to update C2, we perform a north-south traversal
            // recording the cumulative size of traversed nodes
            CNQNode cNode = c0.neighbors[neighbor.w];
            double cSize = cNode.h;
            while (cSize < c0.h) {
              cNode = cNode.neighbors[neighbor.s];
              cSize += cNode.h;
            }
            this.children[quadrant.sw].neighbors[neighbor.w] = cNode;
          }
        }
      }

      // Step 2.2: Updating Cardinal Neighbors of NE sub-Quadrant.
      if (!(this.parent == null || this.neighbors[neighbor.n] == null)) {
        // Quadrant not on north border
        if (this.neighbors[neighbor.n] != null) {
          if (this.neighbors[neighbor.n].w < this.w) {
            CNQNode c0 = this.children[quadrant.nw];
            c0.neighbors[neighbor.n] = this.neighbors[neighbor.n];
            // to update C1, we perform a west-east traversal
            // recording the cumulative size of traversed nodes
            CNQNode cNode = c0.neighbors[neighbor.n];
            double cSize = cNode.w;
            while (cSize < c0.w) {
              cNode = cNode.neighbors[neighbor.e];
              cSize += cNode.w;
            }
            this.children[quadrant.ne].neighbors[neighbor.n] = cNode;
          }
        }
      }

      if (this.neighbors[neighbor.w] != null) {
        Action<CNQNode> opw = delegate (CNQNode node){
            CNQNode western = node;
            if (western.neighbors[neighbor.e] == this) {
              if (western.y + western.h > this.children[quadrant.sw].y) {
                // choose SW
                western.neighbors[neighbor.e] = this.children[quadrant.sw];
              } else {
                // choose NW
                western.neighbors[neighbor.e] = this.children[quadrant.nw];
              }
              if (western.neighbors[neighbor.e].y == western.y) {
                western.neighbors[neighbor.e].neighbors[neighbor.w] = western;
              }
            }
          };
        this.forEachNeighborInDirection(neighbor.w, opw);
      }

      if (this.neighbors[neighbor.n] != null) {
        Action<CNQNode> ops = delegate (CNQNode node){
            CNQNode northern = node;
            if (northern.neighbors[neighbor.s] == this) {
              if (northern.x + northern.w > this.children[quadrant.ne].x) {
                // choose NE
                northern.neighbors[neighbor.s] = this.children[quadrant.ne];
              } else {
                // choose NW
                northern.neighbors[neighbor.s] = this.children[quadrant.nw];
              }
              if (northern.neighbors[neighbor.s].x == northern.x) {
                northern.neighbors[neighbor.s].neighbors[neighbor.n] = northern;
              }
            }
          };
        this.forEachNeighborInDirection(neighbor.n, ops);
      }

      if (this.neighbors[neighbor.e] != null) {
        if (this.neighbors[neighbor.e] != null && this.neighbors[neighbor.e].neighbors[neighbor.w] == this) {
          // To update the eastern CN of a quadrant Q that is being
          // decomposed: Q.CN2.CN0=Q.Ch[NE]
          this.neighbors[neighbor.e].neighbors[neighbor.w] = this.children[quadrant.ne];
        }
      }

      if (this.neighbors[neighbor.s] != null) {
        if (this.neighbors[neighbor.s] != null && this.neighbors[neighbor.s].neighbors[neighbor.n] == this) {
          this.neighbors[neighbor.s].neighbors[neighbor.n] = this.children[quadrant.sw];
        }
      }
    }

    /// <summary>
    /// Perform an action for each neighbor in a specific direction
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="operation"></param>
    /// <returns></returns>
    public void forEachNeighborInDirection(int dir, Action<CNQNode> operation) {
      // start from the cardinal neighbor on the given direction
      CNQNode node = this.neighbors[dir];
      if (node == null) {
        return;
      }
      operation(node);
      if (((dir == neighbor.n || dir == neighbor.s) && node.w >= this.w)
      || ((dir == neighbor.w || dir == neighbor.e) && node.h >= this.h)) {
        //no more neighbors -> end traversal
        return;
      }

      //traverse to other neighbors
      int travDir = traversal[dir];
      int oppDir = opposite[dir];

      CNQNode tNode = node.neighbors[travDir];
      while (tNode != null) {
        if (tNode != null && tNode.neighbors[oppDir] == this) {
          operation(tNode);
        } else {
          break;
        }
        tNode = tNode.neighbors[travDir];
      }
    }

    /// <summary>
    /// Perform an operation for each neighbor
    /// </summary>
    /// <param name="operation"></param>
    /// <returns></returns>
    public void forEachNeighbor(Action<CNQNode> operation) {
      this.forEachNeighborInDirection(neighbor.w, operation);
      this.forEachNeighborInDirection(neighbor.n, operation);
      this.forEachNeighborInDirection(neighbor.e, operation);
      this.forEachNeighborInDirection(neighbor.s, operation);
    }

    /// <summary>
    /// Update exposure values on this node. Only applies to leaf nodes.
    /// </summary>
    /// <returns></returns>
    public void updateExposure () {
      if (this.value != 1){
        //not a leaf
        this.borderLength = -1;
        this.exposure = -1;
        return;
      }

      //Value and action for calculating exposure
      double bLen = 0;
      Action<CNQNode> ops = delegate (CNQNode node){
          int val = node.value;
          if (val < 0){
            val = 0;
          }
          double mult = 0;
          if (node.y < this.y || node.y >= this.y + this.h){ //north or south
            if (node.w > this.w) {
              mult = this.w;
            } else {
              mult = node.w;
            }
          } else { //west or east
            if (node.h > this.h) {
              mult = this.h;
            } else {
              mult = node.h;
            }
          }
          bLen += val * mult;
        };
      //determine exposure by direction
      this.forEachNeighborInDirection(neighbor.w, ops);
      this.borderLengthW = bLen;
      bLen = 0;

      this.forEachNeighborInDirection(neighbor.n, ops);
      this.borderLengthN = bLen;
      bLen = 0;

      this.forEachNeighborInDirection(neighbor.e, ops);
      this.borderLengthE = bLen;
      bLen = 0;

      this.forEachNeighborInDirection(neighbor.s, ops);
      this.borderLengthS = bLen;

      //sum up border lenght and calculate exposure
      this.borderLength = this.borderLengthW + this.borderLengthN + this.borderLengthE + this.borderLengthS;

      this.exposure = (this.borderLength / (2 * this.w + 2 * this.h));
    }
  }

  /// <summary>
  ///Cardinal Neighbor Quad Tree data structure for subdividing parcel areas.
  ///
  ///Partially based on "Region quadtrees in Go" by Aurelien Rainone 2017
  ///https://github.com/aurelien-rainone/go-rquad
  /// </summary>
  public class CNQuadTree {
    //Origin ("lower left" corner) of subdivision area
    public Point3d p0;
    //Width and Height of subdivision area (may differ in non-square case)
    public double w;
    public double h;

    //area of white nodes
    public double area;

    //W direction vector (X-Axis direction)
    public Vector3d dirW;
    //H direction vector (Y-Axis direction)
    public Vector3d dirH;
    //Input area
    public List<Brep> poly;
    //Length limit for subdivision Quad side (w or h)
    public double limit = 1;
    //Overall subdivision depth limit of the tree
    public int maxDepth;
    //Relative subdivision limit (span actually occurring leaf sizes)  
    public int depthLimit;
    //Minimal depth with a white node
    public int minDepth;

    //Dictionary of all Nodes
    public Dictionary<int,CNQNode> nodes;

    //List of white Quad geometries
    public List<Curve> geoms;
    //List of outer rings of the input area
    public List<Curve> polyOuter;
    //List of inner rings of the input area (i.e. holes)
    public List<Curve> polyInner;
    //Root node
    public CNQNode root;
    //Comparison tolerance
    public double tolerance;
    //White nodes by Z-Order ID
    public Dictionary<int,CNQNode> whiteNodes;
    //Equivalence list for determining connected components
    public Dictionary<int,int> connectedComponentEqiuvalence;
    //Connected components by ID
    public Dictionary<int,HashSet<CNQNode>> connectedComponents;
    //Area values of connected components by ID
    public Dictionary<int,double> connectedComponentAreas;

    /// <summary>
    /// Create a new CNQuadTree
    /// </summary>
    /// <param name="p0">Origin (lower left corner) of subdivision area</param>
    /// <param name="w">with of subdivision area (x in direction)</param>
    /// <param name="h">height of subdivision area (y in direction)</param>
    /// <param name="dirW">W direction vector (X-Axis direction)</param>
    /// <param name="dirH">H direction vector (Y-Axis direction)</param>
    /// <param name="poly">Brep representing the input area to be subdivided</param>
    /// <param name="lim">subdivision length limit for quads (w or h)</param>
    /// <param name="depthLimit">relative subdivision limit (span actually occurring leaf sizes)</param>
    /// <param name="maxDepth">maximum level of subdivision for the constructed tree</param>
    /// <param name="tolerance">comparison tolerance</param>
    public CNQuadTree(Point3d p0, double w, double h, Vector3d dirW, Vector3d dirH, List<Brep> poly, double lim, int depthLimit, int maxDepth, double tolerance){
      this.nodes = new Dictionary<int,CNQNode>();
      this.geoms = new List<Curve>();
      this.whiteNodes = new Dictionary<int,CNQNode>();

      this.area = 0;

      this.p0 = p0;
      this.w = w;
      this.h = h;
      this.dirW = dirW;
      this.dirH = dirH;
      this.limit = lim;
      this.maxDepth = maxDepth;
      this.depthLimit = depthLimit;
      this.minDepth = System.Int16.MaxValue;
      this.poly = poly;
      this.tolerance = tolerance;

      //determine inner and outer rings from input area
      this.polyOuter = new List<Curve>();
      this.polyInner = new List<Curve>();
      foreach (Brep p in poly) {
        foreach (BrepLoop lp in p.Loops){
          var crv = lp.To3dCurve();
          if (lp.LoopType == BrepLoopType.Outer){
            this.polyOuter.Add(crv);
          } else if (lp.LoopType == BrepLoopType.Inner){
            this.polyInner.Add(crv);
          }
        }
      }

      //perform subdivision
      this.buildCNTree(p0, w, h);
    }

    /// <summary>
    /// Test whether the given geometry is completely inside the input area
    /// </summary>
    /// <param name="rect"></param>
    /// <returns></returns>
    public int testContainment(Curve rect) {
      int containment = 0;

      Plane plane = Plane.WorldXY;
      //Check if inside an outer loop
      for(int i = 0; i < this.polyOuter.Count;i++){
        Curve loop = this.polyOuter[i];

        RegionContainment relationship = Curve.PlanarClosedCurveRelationship(rect, loop, plane, this.tolerance);
        if (relationship == RegionContainment.AInsideB){
          //completely within a outer boundary
          containment = 1;
          break;
        } else if (relationship == RegionContainment.MutualIntersection) {
          //intersecting an outer boundary -> children may be conteined
          containment = -1;
          break;
        } else if (relationship == RegionContainment.Disjoint) {
          //outside outer boundary
          containment = 0;
        }
      }

      if (containment != 0){
        //Check if outside all inner loops
        foreach(Curve loop in this.polyInner){
          RegionContainment relationship = Curve.PlanarClosedCurveRelationship(rect, loop, plane, this.tolerance);
          if (!(relationship == RegionContainment.Disjoint)) {
            if (relationship == RegionContainment.AInsideB){
              //completely within a hole
              containment = 0;
              break;
            } else if (relationship == RegionContainment.MutualIntersection) {
              //intersecting a hole -> children may be contained
              containment = -1;
              break;
            }
            return containment;
          }
        }
      }

      return containment;
    }

    /// <summary>
    /// Constuct Quad rectangle geometry as PolylineCurve
    /// </summary>
    /// <param name="p0">corner point</param>
    /// <param name="w">width</param>
    /// <param name="h">height</param>
    /// <returns></returns>
    public Curve createRectGeom(Point3d p0, double w, double h) {
      Vector3d v1 = Vector3d.Multiply(dirW, w);
      Vector3d v3 = Vector3d.Multiply(dirH, h);
      Vector3d v2 = Vector3d.Add(v1, v3);

      Point3d p1 = Point3d.Add(p0, v1);
      Point3d p2 = Point3d.Add(p0, v2);
      Point3d p3 = Point3d.Add(p0, v3);

      Point3d[] corners = new Point3d[]{p0,p1,p2,p3,p0};

      return new PolylineCurve(corners);
    }

    /// <summary>
    /// Collect all geometries from the tree based in a condition.
    /// </summary>
    /// <param name="condition"></param>
    /// <returns></returns>
    public List<Curve> collectGeometries(Func<CNQNode,bool> condition) {
      List<Curve> geoms = new List<Curve>();
      foreach(CNQNode node in this.nodes.Values){
        if(condition(node)){
          geoms.Add(node.rect);
        }
      }
      return geoms;
    }

    /// <summary>
    /// Visit all nodes Post Order and collect the nodes in the specified 
    /// List based on a condition.
    /// </summary>
    /// <param name="node"></param>
    /// <param name="condition"></param>
    /// <param name="nodes"></param>
    /// <returns></returns>
    public void visitPostOrder(CNQNode node, Func<CNQNode,bool> condition, List<CNQNode> nodes) {
      //visit children first
      foreach(CNQNode child in node.children) {
        if (child != null) {
          visitPostOrder(child, condition, nodes);
        }
      }
      //visit parent after
      visitNode(node, condition, nodes);
    }

    /// <summary>
    /// Visit all nodes In Order and collect the nodes in the specified 
    /// List based on a condition.
    /// </summary>
    /// <param name="node"></param>
    /// <param name="condition"></param>
    /// <param name="nodes"></param>
    /// <returns></returns>
    public void visitInOrder(CNQNode node, Func<CNQNode,bool> condition, List<CNQNode> nodes) {
      if(node == null){
        return;
      }
      //visit parent first
      visitNode(node, condition, nodes);
      //visit childrren after
      foreach(CNQNode child in node.children) {
        visitInOrder(child, condition, nodes);
      }
    }

    /// <summary>
    /// Add a node to a list based on the specified condition
    /// </summary>
    /// <param name="node"></param>
    /// <param name="condition"></param>
    /// <param name="nodes"></param>
    /// <returns></returns>
    private void visitNode(CNQNode node, Func<CNQNode,bool> condition, List<CNQNode> nodes) {
      if (condition(node)){
        nodes.Add(node);
      }
    }

    /// <summary>
    /// Calculate the Z-Order ID of a Quad based in the parent and the quadrant
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="quadrant"></param>
    /// <returns></returns>
    public int generateZOrder (CNQNode parent, int quadrant) {
      if (parent == null){
        return 0;
      }
      int shift = 2 * (this.maxDepth - parent.depth);
      return parent.quad | (quadrant << shift);
    }

    /// <summary>
    /// Initiate the tree construction
    /// </summary>
    /// <param name="p0">origin of the subdivision area</param>
    /// <param name="w">width of the subdivision area</param>
    /// <param name="h">height of the subdivision area</param>
    /// <returns></returns>
    public void buildCNTree(Point3d p0, double w, double h) {
      //Create root node spanning the entire subdivision area
      this.root = this.createAndInsertNode(null, quadrant.nw, p0, w, h, 0, 0);

      //Initiate the recursive subdivision process
      this.splitNode(this.root);
    }

    /// <summary>
    /// Create a new node and add it to the tree
    /// </summary>
    /// <param name="parent">parent  node</param>
    /// <param name="quadrant">quadrant inside the parent</param>
    /// <param name="p0">origin of the node quad</param>
    /// <param name="w">width of the quad</param>
    /// <param name="h">height of the quad</param>
    /// <param name="x">x-coordinate</param>
    /// <param name="y">y-coordinate</param>
    /// <returns></returns>
    public CNQNode createAndInsertNode(CNQNode parent, int quadrant, Point3d p0, double w, double h, double x, double y) {
      Curve rect = createRectGeom(p0, w, h);
      int zOrder = this.generateZOrder(parent, quadrant);

      //determine value
      sbyte value = -1;

      if (parent == null){
        //root -> "grey" value
        value = -1;
      } else if(parent.value != -1){
        //parent is uniform -> inherit value
        //irregular case during intentional split in post processing
        value = parent.value;
      } else if((w > this.limit || h > this.limit) && parent.depth <= this.maxDepth){
        //determine value by containment
        int containment = this.testContainment(rect);

        if (containment == 1) {
          //inside
          this.geoms.Add(rect);
          value = 1;
        } else  if (containment == 0) {
          //outside
          value = 0;
        } else if ((w / 2 > this.limit || h / 2 > this.limit) && parent.depth + 1 <= this.maxDepth) {
          //interior
          value = -1;
        } else {
          //declared outside due to limits
          value = 0;
        }
      } else {
        value = 0;
      }

      //actually create node and add to tree data structures
      CNQNode node = new CNQNode(parent, zOrder, value, p0, x, y, w, h, rect);
      this.nodes[zOrder] = node;
      if (value == 1){
        this.whiteNodes[node.quad] = node;
        this.area += node.area;
        if (node.depth < this.minDepth){
          this.minDepth = node.depth;
        }
      }
      return node;
    }

    /// <summary>
    /// Subdivide node
    ///
    /// Compare Steps in
    /// QASEM, S. W. & TOUIR, A. A. 2015.
    /// Cardinal Neighbor Quadtree:
    /// a New Quadtree-based Structure for Constant-Time Neighbor Finding.
    /// International Journal of Computer Applications, 132(8), 22-30.
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    public void splitNode(CNQNode node) {
      if (node.depth == this.maxDepth) {
        return;
      }

      //Step 1
      //new width and height
      double w2 = node.w / 2;
      double h2 = node.h / 2;
      //Cectors for children origins
      Vector3d v1 = Vector3d.Multiply(this.dirW, w2);
      Vector3d v2 = Vector3d.Multiply(this.dirH, h2);
      Vector3d v3 = Vector3d.Add(v1, v2);

      //Generating child nodes
      //Rhino.RhinoApp.WriteLine("NW");
      CNQNode nw = this.createAndInsertNode(node, quadrant.nw, node.p0, w2, h2, node.x, node.y);
      //Rhino.RhinoApp.WriteLine("NE");
      CNQNode ne = this.createAndInsertNode(node, quadrant.ne, Point3d.Add(node.p0, v1), w2, h2, node.x + w2, node.y);
      //Rhino.RhinoApp.WriteLine("SW");
      CNQNode sw = this.createAndInsertNode(node, quadrant.sw, Point3d.Add(node.p0, v2), w2, h2, node.x, node.y + h2);
      //Rhino.RhinoApp.WriteLine("SE");
      CNQNode se = this.createAndInsertNode(node, quadrant.se, Point3d.Add(node.p0, v3), w2, h2, node.x + w2, node.y + h2);

      //at creation, each sub-quadrant first inherits its parent external neighbors
      nw.neighbors[neighbor.w] = node.neighbors[neighbor.w];
      nw.neighbors[neighbor.n] = node.neighbors[neighbor.n];
      nw.neighbors[neighbor.e] = ne;
      nw.neighbors[neighbor.s] = sw;

      ne.neighbors[neighbor.w] = nw;
      ne.neighbors[neighbor.n] = node.neighbors[neighbor.n];
      ne.neighbors[neighbor.e] = node.neighbors[neighbor.e];
      ne.neighbors[neighbor.s] = se;

      sw.neighbors[neighbor.w] = node.neighbors[neighbor.w];
      sw.neighbors[neighbor.n] = nw;
      sw.neighbors[neighbor.e] = se;
      sw.neighbors[neighbor.s] = node.neighbors[neighbor.s];

      se.neighbors[neighbor.w] = sw;
      se.neighbors[neighbor.n] = ne;
      se.neighbors[neighbor.e] = node.neighbors[neighbor.e];
      se.neighbors[neighbor.s] = node.neighbors[neighbor.s];

      node.children[quadrant.nw] = nw;
      node.children[quadrant.ne] = ne;
      node.children[quadrant.sw] = sw;
      node.children[quadrant.se] = se;

      //Not a leaf anymore
      node.value = -1;

      //Step 2 and Step 3
      node.updateNeighbors();

      // subdivide non-leaf nodes
      if (node.depth + 1 < this.maxDepth
      && (this.minDepth == System.Int16.MaxValue || node.depth - this.minDepth + 1 < this.depthLimit)) {
        if (nw.value == -1) {
          this.splitNode(nw);
        }
        if (ne.value == -1) {
          this.splitNode(ne);
        }
        if (sw.value == -1) {
          this.splitNode(sw);
        }
        if (se.value == -1) {
          this.splitNode(se);
        }
      } else {
        //declare external/blach due to limits

        if (nw.value == -1) {
          nw.value = 0;
        }
        if (ne.value == -1) {
          ne.value = 0;
        }
        if (sw.value == -1) {
          sw.value = 0;
        }
        if (se.value == -1) {
          se.value = 0;
        }
      }
    }

    /// <summary>
    /// Intentionally turn a node black/external. Intended for postprocessing.
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    public void turnNodeBlack(CNQNode node) {
      if (node.value != 1){
        return;
      }
      node.value = 0;
      this.whiteNodes.Remove(node.quad);
    }

    /// <summary>
    /// Determine connected components of white nodes in the tree.
    ///
    /// Derived from 
    /// SAMET, H. 1981. 
    /// Connected Component Labeling Using Quadtrees. 
    /// Journal of the ACM (JACM), 28(3), 487-501.
    /// </summary>
    /// <returns></returns>
    public void determineConnectedComponents () {
      //Data structures for labeling
      this.connectedComponentEqiuvalence = new Dictionary<int,int>();
      this.connectedComponents = new Dictionary<int,HashSet<CNQNode>>();
      this.connectedComponentAreas = new Dictionary<int,double>();

      //Function implementing the labeling steps when visiting the nodes
      Func < CNQNode,bool> visitLabel = delegate (CNQNode node){
          if (node.value != 1) {
            //black or internal
            return false;
          }

          //get component labels frim neighbors in N and W direction
          HashSet<int> components = new HashSet<int>();
          Action <CNQNode> getComponent = delegate(CNQNode vNode) {
              if(vNode.value == 1 && vNode.connectedComponent != -1){
                components.Add(vNode.connectedComponent);
              }
            };

          node.forEachNeighborInDirection(neighbor.n, getComponent);
          node.forEachNeighborInDirection(neighbor.w, getComponent);

          //determine label
          if (components.Count == 0){ //new component
            node.connectedComponent = node.quad;
          } else if (components.Count == 1){ //one existing component
            node.connectedComponent = components.First();
          } else {
            int minComponent = components.Min();
            node.connectedComponent = minComponent;
            foreach(int c in components) {
              this.connectedComponentEqiuvalence[c] = minComponent;
            }
          }

          return true;
        };

      //label al nodes
      List<CNQNode> visitedNodes = new List<CNQNode>();
      this.visitPostOrder(this.root, visitLabel, visitedNodes);

      //determine equivalent components
      Dictionary<int,int> ccEquivNormalized = new Dictionary<int,int>(this.connectedComponentEqiuvalence);
      var sortedKeys = this.connectedComponentEqiuvalence.Keys.ToList();
      sortedKeys.Sort();
      foreach(int key in sortedKeys) {
        int prevKey = key;
        int cKey = this.connectedComponentEqiuvalence[key];
        while(prevKey != cKey) {
          prevKey = cKey;
          cKey = this.connectedComponentEqiuvalence[prevKey];
        }
        ccEquivNormalized[key] = cKey;
      }

      //merge equivalent components
      this.connectedComponentEqiuvalence = ccEquivNormalized;

      foreach(CNQNode node in visitedNodes) {
        //initial component label
        int equivalentComponent = node.connectedComponent; 
        //determine equivalent label
        if(this.connectedComponentEqiuvalence.ContainsKey(node.connectedComponent)){
          equivalentComponent = this.connectedComponentEqiuvalence[node.connectedComponent];
        }
        node.connectedComponent = equivalentComponent;
        if (this.connectedComponents.ContainsKey(equivalentComponent)){
          //add to existing component
          this.connectedComponents[equivalentComponent].Add(node);
          this.connectedComponentAreas[equivalentComponent] += node.area;
        } else {
          //add new component for label
          HashSet<CNQNode> component = new HashSet<CNQNode>();
          component.Add(node);
          this.connectedComponents[equivalentComponent] = component;
          this.connectedComponentAreas[equivalentComponent] = node.area;
        }
      }
    }

    /// <summary>
    /// Calculate fill ratios (white ares to total area) for nodes in the tree.
    /// Also updates the exposure values of the nodes
    /// </summary>
    /// <returns></returns>
    public void determineFillRatio () {
      //Function for visiting nodes
      Func <CNQNode,bool> visitLabel = delegate (CNQNode node){
          if (node.value == 1) {
            //white leaf
            node.filledArea = node.area;
            node.updateExposure();
          } else if (node.value == 0) {
            //black leaf
            node.filledArea = 0;
            node.borderLength = 0;
          } else {
            //internal node 
            node.borderLength = 0;
            node.filledArea = 0;
            //sum up values of children (possible since post order)
            foreach(CNQNode c in node.children) {
              if (c != null) {
                node.filledArea += c.filledArea;
                node.borderLength += c.borderLength;
              }
            }
          }

          //calculate actual ratio
          node.filledAreaRatio = node.filledArea / node.area;

          return true;
        };

      //Visit nodes post order
      List<CNQNode> visitedNodes = new List<CNQNode>();
      this.visitPostOrder(this.root, visitLabel, visitedNodes);
    }
  }

  // </Custom additional code> 
}