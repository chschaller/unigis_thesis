
import sys

#import and use custom module
import Rhino
import Rhino.Geometry as rg
import rhinoscriptsyntax  as rs
import scriptcontext as sc

import random
import math
import time

from collections import namedtuple

########## Algorithm constants ##########
# step size for the aspect ratio
aspectRatioStep = 0.5
# step size for angles (in degrees); has linear impact on running time
angleStep = 5
#######################################

###http://rosettacode.org/wiki/Category:Python
Pt = namedtuple('Pt', 'x, y')               # Point
Edge = namedtuple('Edge', 'a, b')           # Polygon edge from a to b
Poly = namedtuple('Poly', 'name, edges')    # Polygon
 
_eps = 0.00001
_huge = sys.float_info.max
_tiny = sys.float_info.min
#####


#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Helper functions
#------------------------------------------------------------------------------

# Returns the squared euclidean distance between points a and b
def squaredDist(a, b):
    deltax = b[0] - a[0]
    deltay = b[1] - a[1]
    return deltax*deltax + deltay*deltay

# Checks whether the horizontal ray going through point p intersects the segment p1p2
# Implementation from: http://rosettacode.org/wiki/Ray-casting_algorithm#CoffeeScript
def rayIntersectsSegment(p, p1, p2):
    [a, b] = [p1, p2] if p1[1] < p2[1] else [p2, p1]
    if p[1] == b[1] or p[1] == a[1]:
        p[1] += _tiny
    if p[1] > b[1] or p[1] < a[1]: return False
    elif p[0] > a[0] and p[0] > b[0]: return False
    elif p[0] < a[0] and p[0] < b[0]: return True
    else:
        mAB = (b[1] - a[1]) / (b[0] - a[0])
        mAP = (p[1] - a[1]) / (p[0] - a[0])
        return mAP > mAB

# Checks whether the point p is inside a polygon using the Ray-Casting algorithm
# Implementation from: http://rosettacode.org/wiki/Ray-casting_algorithm#CoffeeScript
def pointInPoly(p, poly):
    i = 0
    n = poly.PointCount
    b = poly.Point(n-2)
    c = 0
    while i < n:
        a = b
        b = poly.Point(i)
        if rayIntersectsSegment(p, a, b): 
            c = c+1
        i = i+1
    return not(c%2 == 0)

# Checks whether the point p is inside the bounding box of the line segment p1q1
def pointInSegmentBox(p, p1, q1):
    # allow for some margins due to numerical errors
    eps = 1e-9
    [px, py, pz] = p
    if (px < min(p1[0], q1[0]) - eps or
      px > max(p1[0], q1[0]) + eps or
      py < min(p1[1], q1[1]) - eps or
      py > max(p1[1], q1[1]) + eps):
        return False 
    return True

# Finds the intersection point (if there is one) of the lines p1q1 and p2q2
def lineIntersection(p1, q1, p2, q2):
    # allow for some margins due to numerical errors
    eps = 1e-9
    # find the intersection point between the two infinite lines
    dx1 = p1[0] - q1[0]
    dy1 = p1[1] - q1[1]
    dx2 = p2[0] - q2[0]
    dy2 = p2[1] - q2[1]
    denom = dx1 * dy2 - dy1 * dx2
    if abs(denom) < eps:
        return None
    cross1 = p1[0]*q1[1] - p1[1]*q1[0]
    cross2 = p2[0]*q2[1] - p2[1]*q2[0]

    px = (cross1*dx2 - cross2*dx1) / denom
    py = (cross1*dy2 - cross2*dy1) / denom
    return rg.Point3d(px, py, 0.0)

# Checks whether the line segments p1q1 and p2q2 intersect
def segmentsIntersect(p1, q1, p2, q2):
    p = lineIntersection(p1, q1, p2, q2)
    if not p:
        return False 
    return pointInSegmentBox(p, p1, q1) and pointInSegmentBox(p, p2, q2)

# Check if polygon polyA is inside polygon polyB
def polyInsidePoly(polyA, polyB):
    iA = 0
    nA = polyA.PointCount
    nB = polyB.PointCount
    bA = polyA.Point(nA-2)

    while iA < nA:
      aA = bA
      bA = polyA.Point(iA)

      iB = 0
      bB = polyB.Point(nB-2)
      while iB < nB:
        aB = bB
        bB = polyB.Point(iB)
        if segmentsIntersect(aA, bA, aB, bB):
            return False
        iB = iB+1
      iA = iA+1
    return pointInPoly(polyA.Point(0), polyB)

# Rotates the point p for alpha radians around the origin
def rotatePoint(p, alpha, origin):
    origin = origin if origin else rg.Point3d(0.0, 0.0, 0.0)
    xshifted = p[0] - origin[0]
    yshifted = p[1] - origin[1]
    cosAlpha = math.cos(alpha)
    sinAlpha = math.sin(alpha)
    return rg.Point3d( cosAlpha * xshifted - sinAlpha * yshifted + origin[0],
           sinAlpha * xshifted + cosAlpha * yshifted + origin[1], 0.0)

# Rotates the polygon for alpha radians around the origin
def rotatePoly(poly, alpha, origin):
    rotatedPoly = []
    for i in range(poly.PointCount):
        rotatedPoly.append(rotatePoint(poly.Point(i), alpha, origin))
    return rg.PolylineCurve(rotatedPoly)


# Gives the 2 closest intersection points between a ray with alpha radians
# from the origin and the polygon. The two points should lie on opposite sides of the origin
def intersectPoints(poly, origin, alpha):
    eps = 1e-9
    origin = rg.Point3d(origin[0] + eps*math.cos(alpha), origin[1] + eps*math.sin(alpha), 0.0)
    [x0, y0, z0] = origin
    shiftedOrigin = rg.Point3d(x0 + math.cos(alpha), y0 + math.sin(alpha), 0.0)

    idx = 0
    if abs(shiftedOrigin[0] - x0) < eps:
        idx = 1
    i = 0
    n = poly.PointCount
    b = poly.Point(n-2)
    minSqDistLeft = _huge
    minSqDistRight = _huge
    closestPointLeft = None
    closestPointRight = None
    while i < n:
        a = b
        b = poly.Point(i)
        p = lineIntersection(origin, shiftedOrigin, a, b)
        if p and pointInSegmentBox(p, a, b):
             sqDist = squaredDist(origin, p)
             if p[idx] < origin[idx]:
                 if sqDist < minSqDistLeft:
                     minSqDistLeft = sqDist
                     closestPointLeft = p
             elif p[idx] > origin[idx]:
                 if sqDist < minSqDistRight:
                     minSqDistRight = sqDist
                     closestPointRight = p
        i = i+1
    return [closestPointLeft, closestPointRight]

###https://stackoverflow.com/a/10986098
def seq(start, stop, step=1):
    n = int(round((stop - start)/float(step)))
    if n > 1:
        return([start + step*i for i in range(n+1)])
    else:
        return([])


####http://rosettacode.org/wiki/Category:Python
def _odd(x): 
    return x%2 == 1
 
def ispointinside(p, poly):
    ln = len(poly)
    return _odd(sum(rayintersectseg(p, edge)
                    for edge in poly.edges ))

start = time.time()
##faked inputs
poly = rg.PolylineCurve([ rg.Point3d(3.0,0.0,0.0),rg.Point3d(7.0,0.0,0.0),rg.Point3d(10.0,5.0,0.0),
    rg.Point3d(7.0,10.0,0.0),rg.Point3d(3.0,10.0,0.0),
    rg.Point3d(0.0,5.0,0.0),rg.Point3d(3.0,0.0,0.0)
])
origin = []

angle = []
aspectRatio = []
maxAspectRatio = None
minWidth = None
minHeight = None
tolerance = None
nTries = None

if poly.PointCount <= 3 or not poly.IsClosed :
    print("Error")
    ##return None

##### User's input normalization #####
# maximum allowed aspect ratio for the rectangle solution
maxAspectRatio = maxAspectRatio if maxAspectRatio else 15

minWidth = minWidth if minWidth else 0
minHeight = miHeight if minHeight else 0
tolerance = tolerance if tolerance else 0.02
nTries = nTries if nTries else 20 # Default value for the number of possible center points of the maximal rectangle

angles = angle if len(angle) else range(-90,90+angleStep, angleStep)

aspectRatios = aspectRatio 

origins = origin


########################################
areaMassProperties = rg.AreaMassProperties.Compute(poly)
area = abs(areaMassProperties.Area)
if area == 0:
    print("polygon has 0 area")
    #return null
#get the width of the bounding box of the original polygon to determine tolerance
bBox = poly.GetBoundingBox(True)
[minx, maxx] = [bBox.Min.X, bBox.Max.X]
[miny, maxy] = [bBox.Min.Y, bBox.Max.Y]

# get the width of the bounding box of the simplified polygon
[boxWidth, boxHeight] = [maxx - minx, maxy - miny]

# discretize the binary search for optimal width to a resolution of this times the polygon width
widthStep = min(boxWidth, boxHeight)/50

# populate possible center points with random points inside the polygon
if not origins:
    origins = []
    # get the centroid of the polygon
    centroid = areaMassProperties.Centroid
    if pointInPoly(centroid, poly):
        origins.append(centroid)
    # get few more points inside the polygon
    while len(origins) < nTries:
        rndX = random.random() * boxWidth + minx
        rndY = random.random() * boxHeight + miny
        rndPoint = rg.Point3d(rndX, rndY, 0.0)
        if pointInPoly(rndPoint, poly):
            origins.append(rndPoint)

maxArea = 0
maxRect = None
for angle in angles:
    angleRad = -angle*math.pi/180

    for i in range(len(origins)):
        origOrigin = origins[i]
        # generate improved origins
        [p1W, p2W] = intersectPoints(poly, origOrigin, angleRad)
        [p1H, p2H] = intersectPoints(poly, origOrigin, angleRad + math.pi/2)

        modifOrigins = []
        if p1W and p2W:
            modifOrigins.append(rg.Point3d((p1W[0] + p2W[0])/2, (p1W[1] + p2W[1])/2, 0.0)) # average along with width axis
        if p1H and p2H:
            modifOrigins.append(rg.Point3d((p1H[0] + p2H[0])/2, (p1H[1] + p2H[1])/2, 0.0)) # average along with height axis

        for origin in modifOrigins:

            [p1W, p2W] = intersectPoints(poly, origin, angleRad)
            if not p1W or not p2W:
                continue
            minSqDistW = min(squaredDist(origin, p1W), squaredDist(origin, p2W))
            maxWidth = 2*math.sqrt(minSqDistW)

            [p1H, p2H] = intersectPoints(poly, origin, angleRad + math.pi/2)
            if not p1H or not p2H:
                continue
            minSqDistH = min(squaredDist(origin, p1H), squaredDist(origin, p2H))
            maxHeight = 2*math.sqrt(minSqDistH)

            if maxWidth * maxHeight < maxArea:
                continue

            if aspectRatios:
                aRatios = aspectRatios
            else:
                minAspectRatio = max(1, minWidth / maxHeight, maxArea/(maxHeight*maxHeight))
                maxAspectRatio = min(maxAspectRatio, (maxWidth/minHeight if not minHeight == 0 else _huge), ((maxWidth*maxWidth)/maxArea if not maxArea == 0 else _huge))
                aRatios = seq(minAspectRatio, maxAspectRatio + aspectRatioStep, aspectRatioStep)

            for aRatio in aRatios:
                # do a binary search to find the max width that works
                left = max(minWidth, math.sqrt(maxArea*aRatio))
                right = min(maxWidth, maxHeight*aRatio)
                if right * maxHeight < maxArea:
                    continue

                #if (right - left) >= widthStep:
                  #print('aRatio', aRatio)

                while (right - left) >= widthStep:
                    width = (left + right) / 2
                    height = width / aRatio
                    [x0, y0, z0] = origin
                    rectPolyPoints = [
                      rg.Point3d(x0 - width/2, y0 - height/2, 0.0),
                      rg.Point3d(x0 + width/2, y0 -  height/2, 0.0),
                      rg.Point3d(x0 + width/2, y0 + height/2, 0.0),
                      rg.Point3d(x0 - width/2, y0 + height/2, 0.0)
                    ]
                    rectPolyPoints.append(rectPolyPoints[0])
                    rectPoly = rg.PolylineCurve(rectPolyPoints)
                    rectPoly = rotatePoly(rectPoly, angleRad, origin)
                    if polyInsidePoly(rectPoly, poly):
                        insidePoly = True
                        # we know that the area is already greater than the maxArea found so far
                        maxArea = width * height
                        maxRect = dict(
                            poly=rectPoly,
                            cx=x0,
                            cy=y0,
                            width=width,
                            height=height,
                            angle=angle)
                        left = width # increase the width in the binary search
                    else:
                        insidePoly = False
                        right = width # decrease the width in the binary search

end = time.time()
print(end-start)
print(maxRect)

#  return [maxRect, maxArea, events]