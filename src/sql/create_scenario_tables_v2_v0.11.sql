﻿--
--Function for updating timestamp column 
--Basing on https://www.revsys.com/blog/2006/aug/04/automatically-updating-a-timestamp-column-in-postgresql/
--
CREATE OR REPLACE FUNCTION update_modified_column()   
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified = now();
    RETURN NEW;   
END;
$$ language 'plpgsql';

--
-- Create Schema: dencity
--
--DROP SCHEMA IF EXISTS  dencity;
CREATE SCHEMA IF NOT EXISTS dencity
  AUTHORIZATION postgres;

--
--Drop Scenario tables
--
DROP TABLE IF EXISTS dencity.strasse;
DROP TABLE IF EXISTS dencity.gebaeude;
DROP TABLE IF EXISTS dencity.parzelle;
DROP TABLE IF EXISTS dencity.abstandslinie;
DROP TABLE IF EXISTS dencity.szenario_parameter;
DROP TABLE IF EXISTS dencity.szenario;
DROP TABLE IF EXISTS dencity.projekt;

--
--Create Scenario tables
--

--
--Projekt table
--
CREATE TABLE dencity.projekt
(
  id serial NOT NULL,
  titel character varying(30),
  beschreibung character varying(512),
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT projekt_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.projekt
  OWNER TO postgres;

CREATE TRIGGER update_projekt_modtime BEFORE UPDATE ON dencity.projekt FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

--
--Base Szenario table
--
CREATE TABLE dencity.szenario
(
  id serial NOT NULL,
  titel character varying(30),
  beschreibung character varying(512),
  pid int NOT NULL REFERENCES dencity.projekt (id), 
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT szenario_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.szenario
  OWNER TO postgres;

CREATE TRIGGER update_szenario_modtime BEFORE UPDATE ON dencity.szenario FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

--
--Szenario Parameter table
--
CREATE TABLE dencity.szenario_parameter
(
  id serial NOT NULL,
  sid int NOT NULL REFERENCES dencity.szenario (id), 
  zone_code character varying(100) NOT NULL,
  parameter_name character varying(100) NOT NULL,
  parameter_wert character varying(1024) NOT NULL,
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT szenario_parameter_unique UNIQUE (sid, zone_code, parameter_name),
  CONSTRAINT szenario_parameter_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.szenario_parameter
  OWNER TO postgres;

CREATE TRIGGER update_szenario_parameter_modtime BEFORE UPDATE ON dencity.szenario_parameter FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

--
--Parzelle tables
--
CREATE TABLE dencity.parzelle
(
  gid serial NOT NULL,
  pid int NOT NULL REFERENCES dencity.projekt (id), 
  nummer character varying(15),
  zone_code character varying(100),
  anrechenbare_grundstuecksflaeche real NOT NULL,
  geom geometry(MultiPolygon,2056),
  CONSTRAINT parzelle_nummer_unique UNIQUE (pid, nummer),
  CONSTRAINT parzelle_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.parzelle
  OWNER TO postgres;

CREATE INDEX parzelle_geom_idx
  ON dencity.parzelle
  USING gist (geom);
  
SELECT Populate_Geometry_Columns('dencity.parzelle'::regclass);  

--
--Abstandslinie table
--
CREATE TABLE dencity.abstandslinie
(
  gid serial NOT NULL,
  sid int NOT NULL REFERENCES dencity.szenario (id), 
  gestaltend boolean DEFAULT false NOT NULL,  
  geom geometry(Linestring,2056),
  CONSTRAINT abstandslinie_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.abstandslinie
  OWNER TO postgres;

CREATE INDEX abstandslinie_geom_idx
  ON dencity.abstandslinie
  USING gist (geom);

SELECT Populate_Geometry_Columns('dencity.abstandslinie'::regclass);

--
--Strasse tables
--
CREATE TABLE dencity.strasse
(
  gid serial NOT NULL,
  pid int NOT NULL REFERENCES dencity.projekt (id), 
  typ_code character varying(20),
  geom geometry(MultiPolygon,2056),
  CONSTRAINT strasse_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.strasse
  OWNER TO postgres;

CREATE INDEX strasse_geom_idx
  ON dencity.strasse
  USING gist (geom);
  
SELECT Populate_Geometry_Columns('dencity.strasse'::regclass);  

  
--
--Gebaeude tables
--
CREATE TABLE dencity.gebaeude
(
  gid serial NOT NULL,
  sid int NOT NULL REFERENCES dencity.szenario (id), 
  hoehe real DEFAULT 0.0 NOT NULL,
  geschosszahl int DEFAULT 1 NOT NULL,
  geschossflaeche real NOT NULL,
  anteil_arbeitsnutzung real NOT NULL DEFAULT 0.0,
  nutzer_wohnen real NOT NULL,
  nutzer_arbeit real NOT NULL,
  geom geometry(MultiPolygon,2056),
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT gebaeude_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dencity.gebaeude
  OWNER TO postgres;

CREATE INDEX gebaeude_geom_idx
  ON dencity.gebaeude
  USING gist (geom);  
  
CREATE TRIGGER update_gebaeude_modtime BEFORE UPDATE ON dencity.gebaeude FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

SELECT Populate_Geometry_Columns('dencity.gebaeude'::regclass);