--
-- Cleanup
--

DELETE FROM dencity.strasse WHERE pid IN (6,7);
DELETE FROM dencity.parzelle WHERE pid IN (6,7);

DELETE FROM dencity.szenario_parameter WHERE sid IN (SELECT id FROM dencity.szenario WHERE pid IN (6,7));
DELETE FROM dencity.gebaeude WHERE sid IN (SELECT id FROM dencity.szenario WHERE pid IN (6,7));
DELETE FROM dencity.abstandslinie WHERE sid IN (SELECT id FROM dencity.szenario WHERE pid IN (6,7));

DELETE FROM dencity.szenario WHERE pid IN (6,7);

DELETE FROM dencity.projekt WHERE id IN (6,7);

--
--Prepare Projekt Langenthal
--

--Create project entry
INSERT INTO dencity.projekt(id, titel)
    VALUES (6, 'Langenthal v0.11');

--Insert parcels for Langenthal
DELETE FROM dencity.parzelle WHERE pid=6;

INSERT INTO dencity.parzelle(pid, nummer, zone_code, anrechenbare_grundstuecksflaeche, geom)
SELECT pid
  ,CASE WHEN COUNT(*) OVER(PARTITION BY nummer)>1 THEN nummer || '_'  || sub_parcel_number ELSE nummer END AS nummer
  ,zone_code
  ,anrechenbare_grundstuecksflaeche
  ,geom
FROM
(
  SELECT pid
    ,nummer
    ,zone_code
    ,ST_Area(geom) AS anrechenbare_grundstuecksflaeche
    ,ST_Multi(geom) AS geom
    ,ROW_NUMBER() OVER (PARTITION BY pid, nummer ORDER BY ST_Area(geom) ) AS sub_parcel_number
  FROM
  (
    SELECT 
      6 AS pid
      ,nummer AS nummer
      ,zone_lo  AS zone_code
      ,(ST_Dump(ST_CollectionExtract(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(uzp_bauzonen.geom)),3))).geom AS geom
    FROM be.mopube_grundstueck, be.uzp_bauzonen
    WHERE bfsnr = 329 
      AND ST_Intersects(mopube_grundstueck.geom,uzp_bauzonen.geom) 
      AND bfs = 329
      AND ST_Within(mopube_grundstueck.geom,ST_SetSRID(ST_GeomFromText('Polygon ((2625763.06347824446856976 1228953.55572490207850933,
      2625855.84641563845798373 1228973.13377591199241579, 2625959.69520795112475753 1229008.88499949499964714,
      2626030.34643550775945187 1228744.58131086290813982, 2626211.23060244601219893 1228671.80203428328968585,
      2626165.69035335816442966 1228581.14714591205120087, 2626127.81108075240626931 1228401.96541819232515991,
      2626023.53667863551527262 1228396.43249073321931064, 2625977.14520993828773499 1228380.68492796458303928,
      2625864.7842215346172452 1228373.44956128695048392, 2625864.7842215346172452 1228373.44956128695048392,
      2625763.06347824446856976 1228953.55572490207850933))'),2056)) 
      AND nummer NOT IN ('1707','1713','4987','2333','197.04','197.03','2995','2055','4981','4983','1737','4982') --Streets
  ) AS t
  WHERE ST_Area(geom)>1

) AS u
ORDER BY 2;


--Remap Bauzonen to Modellzonen 
UPDATE dencity.parzelle
SET
  zone_code=
    CASE 
      WHEN zone_code = 'Wohnzone W2/ B' THEN 'Z1'
      WHEN zone_code = 'Wohnzone W2/ C' THEN 'Z2'
      WHEN zone_code = 'Mischzone MZ 2' THEN 'Z3'
      WHEN zone_code = 'UeO Nr. 41 "Areal Anliker"' THEN 'Z4'
      WHEN zone_code = 'UeO Nr. 41 "Areal Anliker" Baubereich A'THEN 'Z4'
      WHEN zone_code = 'Zone für öffentliche Nutzung ZöN Art. 77 BauG' THEN 'Z5'
      ELSE 'Z5'
    END
WHERE pid = 6;

--Correct geometry of a specific parcel (removing overlapping slivers)
UPDATE dencity.parzelle
  SET geom = ST_Multi((SELECT geom 
                       FROM (SELECT (ST_Dump(geom)).geom AS geom 
                             FROM dencity.parzelle WHERE pid=6 AND nummer ='2166') AS t 
                       ORDER BY ST_Area(geom) DESC LIMIT 1))
WHERE pid=6 AND nummer ='2166';

--Insert Streets for Langenthal
INSERT INTO dencity.strasse(pid,typ_code,geom)
SELECT  
    6 AS pid,
    '2' AS type_code,
    ST_Multi(ST_CollectionExtract(ST_Intersection(ST_Intersection(mopube_bodenbedeckung.geom,mopube_grundstueck.geom),ST_SetSRID(ST_MakeBox2D(ST_Point(2625324.5,1228239.5),ST_Point(2626454.75,1229092.125)),2056)),3)) AS geom
  FROM be.mopube_bodenbedeckung
  LEFT JOIN be.mopube_grundstueck  ON ST_Intersects(mopube_grundstueck.geom,mopube_bodenbedeckung.geom)
  WHERE 
	ST_Intersects(mopube_bodenbedeckung.geom,ST_SetSRID(ST_MakeBox2D(ST_Point(2625324.5,1228239.5),ST_Point(2626454.75,1229092.125)),2056))
	AND be.mopube_bodenbedeckung.art IN (1);


--
--Prepare project Wohlen: Uettligen 
--

--Create project entry
INSERT INTO dencity.projekt(id, titel)
    VALUES (7, 'Wohlen: Uettligen v0.11');

    
--Insert Parcels for Uettligen
DELETE FROM dencity.parzelle WHERE pid=7;

INSERT INTO dencity.parzelle(pid, nummer, zone_code, anrechenbare_grundstuecksflaeche, geom)
SELECT pid
  ,CASE WHEN COUNT(*) OVER(PARTITION BY nummer)>1 THEN nummer || '_'  || sub_parcel_number ELSE nummer END AS nummer
  ,zone_code
  ,anrechenbare_grundstuecksflaeche
  ,geom
FROM
(
  SELECT pid
    ,nummer
    ,zone_code
    ,ST_Area(geom) AS anrechenbare_grundstuecksflaeche
    ,ST_Multi(geom) AS geom
    ,ROW_NUMBER() OVER (PARTITION BY pid, nummer ORDER BY ST_Area(geom) ) AS sub_parcel_number
  FROM
  (
    SELECT 
      7 AS pid
      ,nummer AS nummer
      ,zone_lo  AS zone_code
      ,(ST_Dump(ST_CollectionExtract(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(uzp_bauzonen.geom)),3))).geom AS geom
    FROM be.mopube_grundstueck, be.uzp_bauzonen
   WHERE bfsnr = 360 
      AND ST_Intersects(mopube_grundstueck.geom,uzp_bauzonen.geom) 
      AND bfs = 360 
      AND zone_lo NOT IN ('Landwirtschaftszone / ungezontes Gebiet')
      AND ST_Within(mopube_grundstueck.geom,ST_SetSRID(ST_GeomFromText('Polygon ((2594707.0389655982144177 1204297.62532898178324103, 
2595275.660759671125561 1203316.75273420615121722, 2595881.24297035858035088 1203410.57533022807911038, 
2596688.68591794185340405 1203910.9625090123154223, 2596103.00547004723921418 1204871.93334099510684609, 
2596103.00547004723921418 1204871.93334099510684609, 2595568.50098361866548657 1204749.67965526948682964, 
2594707.0389655982144177 1204297.62532898178324103))'),2056)) 
      AND nummer NOT IN ('6199','4251','4907','5825') --Streets
  ) AS t
  WHERE ST_Area(geom)>1

) AS u
ORDER BY 2;

--Remap Bauzonen to Modellzonen
UPDATE dencity.parzelle
SET
  zone_code=
    CASE 
      WHEN zone_code = 'Wohnzone 1-geschossig' THEN 'Z1'
      WHEN zone_code = 'Dorfzone, 2 geschossig' THEN 'Z2'
      WHEN zone_code = 'Wohnzone 2-geschossig' THEN 'Z2'
      WHEN zone_code = 'Wohnzone 3-geschossig' THEN 'Z3'
      WHEN zone_code = 'Mischzone' THEN 'Z4'
      WHEN zone_code = 'Mischzone Landwirtschaft' THEN 'Z4'
      WHEN zone_code = 'Zone für öffentliche Nutzung' THEN 'Z5'
      ELSE 'Z5'
    END
WHERE pid = 7;

--Insert Streets for Uettligen
INSERT INTO dencity.strasse(pid,typ_code,geom)
SELECT  
    7 AS pid,
    '2' AS type_code,
    ST_Multi(ST_CollectionExtract(ST_Intersection(ST_Intersection(mopube_bodenbedeckung.geom,mopube_grundstueck.geom),ST_SetSRID(ST_MakeBox2D(ST_Point(2594391.5,1202659.125),ST_Point(2596711.5,1204459.25)),2056)),3)) geom
  FROM be.mopube_bodenbedeckung
  LEFT JOIN be.mopube_grundstueck  ON ST_Intersects(mopube_grundstueck.geom,mopube_bodenbedeckung.geom)
  WHERE 
	ST_Intersects(mopube_bodenbedeckung.geom,ST_SetSRID(ST_MakeBox2D(ST_Point(2594391.5,1202659.125),ST_Point(2596711.5,1204459.25)),2056))
	AND be.mopube_bodenbedeckung.art IN (1);

--Deleting existing values
DELETE FROM dencity.gebaeude WHERE sid IN (
    41,42,43,44,45, --Langenthal
    51,52,53,54 --Uettligen
);


DELETE FROM dencity.abstandslinie WHERE sid IN (
    41,42,43,44,45, --Langenthal
    51,52,53,54 --Uettligen
);

DELETE FROM dencity.szenario_parameter WHERE sid IN (
    41,42,43,44,45, --Langenthal
    51,52,53,54 --Uettligen
);

DELETE FROM dencity.szenario
WHERE id IN (
    41,42,43,44,45, --Langenthal
    51,52,53,54 --Uettligen
);
    
--Create scenario entries for Dencity expert validation
--Langenthal
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (41, 'Evaluation: Basis', 6);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (42, 'Evaluation: Basis + Grünflächenziffer 0.7', 6);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (43, 'Evaluation: Basis ohne Ausnützungsziffer', 6);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (44, 'Evaluation: W3', 6);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (45, 'Evaluation: W3 + Abstandslinien', 6);
    
--Uettligen    
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (51, 'Evaluation: Basis', 7);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (52, 'Evaluation: Basis + Grünflächenziffer 0.7', 7);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (53, 'Evaluation: Basis ohne Ausnützungsziffer', 7);
INSERT INTO dencity.szenario(id, titel, pid)
    VALUES (54, 'Evaluation: W3', 7);

    
--Creating scenario parameter entries for expert validation
--Langenthal: Szenario Basis
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Nutzungsziffer', '0.4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5033","2468"]},
{"typ": "reihe","parzellen_nummern": ["2688","2370"]},
{"typ": "reihe","parzellen_nummern": ["2372","2373"]},
{"typ": "reihe","parzellen_nummern": ["2121","2122"]},
{"typ": "reihe","parzellen_nummern": ["451","4691"]},
{"typ": "reihe","parzellen_nummern": ["2992","2991"]},
{"typ": "reihe","parzellen_nummern": ["2994","2993"]},
{"typ": "reihe","parzellen_nummern": ["2742","1010_1"]},
{"typ": "reihe","parzellen_nummern": ["2196","2464"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["4332","4333","4334"]},
{"typ": "reihe","parzellen_nummern": ["4737","347"]},
{"typ": "reihe","parzellen_nummern": ["4727","346"]},
{"typ": "reihe","parzellen_nummern": ["3897","3912"]},
{"typ": "reihe","parzellen_nummern": ["4139","3951"]},
{"typ": "reihe","parzellen_nummern": ["4041","4042"]},
{"typ": "reihe","parzellen_nummern": ["1339","1338"]},
{"typ": "reihe","parzellen_nummern": ["1550","1319","1221"]},
{"typ": "reihe","parzellen_nummern": ["3335","1217"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["2405_2","2395"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Geschosszahl', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Nutzungsziffer', '6.0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Ausnuetzung_Art', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["1214_1","1214_2"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Gesamthoehe', '17');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (41, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Langenthal: Szenario Basis + GF 0.7
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Nutzungsziffer', '0.4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5033","2468"]},
{"typ": "reihe","parzellen_nummern": ["2688","2370"]},
{"typ": "reihe","parzellen_nummern": ["2372","2373"]},
{"typ": "reihe","parzellen_nummern": ["2121","2122"]},
{"typ": "reihe","parzellen_nummern": ["451","4691"]},
{"typ": "reihe","parzellen_nummern": ["2992","2991"]},
{"typ": "reihe","parzellen_nummern": ["2994","2993"]},
{"typ": "reihe","parzellen_nummern": ["2742","1010_1"]},
{"typ": "reihe","parzellen_nummern": ["2196","2464"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["4332","4333","4334"]},
{"typ": "reihe","parzellen_nummern": ["4737","347"]},
{"typ": "reihe","parzellen_nummern": ["4727","346"]},
{"typ": "reihe","parzellen_nummern": ["3897","3912"]},
{"typ": "reihe","parzellen_nummern": ["4139","3951"]},
{"typ": "reihe","parzellen_nummern": ["4041","4042"]},
{"typ": "reihe","parzellen_nummern": ["1339","1338"]},
{"typ": "reihe","parzellen_nummern": ["1550","1319","1221"]},
{"typ": "reihe","parzellen_nummern": ["3335","1217"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["2405_2","2395"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Geschosszahl', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Nutzungsziffer', '6.0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Ausnuetzung_Art', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["1214_1","1214_2"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Gesamthoehe', '17');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (42, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Langenthal: Szenario Basis Keine Ausnutzung
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5033","2468"]},
{"typ": "reihe","parzellen_nummern": ["2688","2370"]},
{"typ": "reihe","parzellen_nummern": ["2372","2373"]},
{"typ": "reihe","parzellen_nummern": ["2121","2122"]},
{"typ": "reihe","parzellen_nummern": ["451","4691"]},
{"typ": "reihe","parzellen_nummern": ["2992","2991"]},
{"typ": "reihe","parzellen_nummern": ["2994","2993"]},
{"typ": "reihe","parzellen_nummern": ["2742","1010_1"]},
{"typ": "reihe","parzellen_nummern": ["2196","2464"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["4332","4333","4334"]},
{"typ": "reihe","parzellen_nummern": ["4737","347"]},
{"typ": "reihe","parzellen_nummern": ["4727","346"]},
{"typ": "reihe","parzellen_nummern": ["3897","3912"]},
{"typ": "reihe","parzellen_nummern": ["4139","3951"]},
{"typ": "reihe","parzellen_nummern": ["4041","4042"]},
{"typ": "reihe","parzellen_nummern": ["1339","1338"]},
{"typ": "reihe","parzellen_nummern": ["1550","1319","1221"]},
{"typ": "reihe","parzellen_nummern": ["3335","1217"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["2405_2","2395"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Geschosszahl', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Nutzungsziffer', '6.0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Ausnuetzung_Art', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["1214_1","1214_2"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Gesamthoehe', '17');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (43, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Langenthal: Szenario Basis + W3
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5033","2468"]},
{"typ": "reihe","parzellen_nummern": ["2688","2370"]},
{"typ": "reihe","parzellen_nummern": ["2372","2373"]},
{"typ": "reihe","parzellen_nummern": ["2121","2122"]},
{"typ": "reihe","parzellen_nummern": ["451","4691"]},
{"typ": "reihe","parzellen_nummern": ["2992","2991"]},
{"typ": "reihe","parzellen_nummern": ["2994","2993"]},
{"typ": "reihe","parzellen_nummern": ["2742","1010_1"]},
{"typ": "reihe","parzellen_nummern": ["2196","2464"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Nutzungsziffer', '0.75');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["4332","4333","4334"]},
{"typ": "reihe","parzellen_nummern": ["4737","347"]},
{"typ": "reihe","parzellen_nummern": ["4727","346"]},
{"typ": "reihe","parzellen_nummern": ["3897","3912"]},
{"typ": "reihe","parzellen_nummern": ["4139","3951"]},
{"typ": "reihe","parzellen_nummern": ["4041","4042"]},
{"typ": "reihe","parzellen_nummern": ["1339","1338"]},
{"typ": "reihe","parzellen_nummern": ["1550","1319","1221"]},
{"typ": "reihe","parzellen_nummern": ["3335","1217"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Nutzungsziffer', '0.9');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["2405_2","2395"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Geschosszahl', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Nutzungsziffer', '6.0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Ausnuetzung_Art', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["1214_1","1214_2"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Gesamthoehe', '17');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (44, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Langenthal: Szenario Basis + W3 + Abstansdlinien
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5033","2468"]},
{"typ": "reihe","parzellen_nummern": ["2688","2370"]},
{"typ": "reihe","parzellen_nummern": ["2372","2373"]},
{"typ": "reihe","parzellen_nummern": ["2121","2122"]},
{"typ": "reihe","parzellen_nummern": ["451","4691"]},
{"typ": "reihe","parzellen_nummern": ["2992","2991"]},
{"typ": "reihe","parzellen_nummern": ["2994","2993"]},
{"typ": "reihe","parzellen_nummern": ["2742","1010_1"]},
{"typ": "reihe","parzellen_nummern": ["2196","2464"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Nutzungsziffer', '0.75');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["4332","4333","4334"]},
{"typ": "reihe","parzellen_nummern": ["4737","347"]},
{"typ": "reihe","parzellen_nummern": ["4727","346"]},
{"typ": "reihe","parzellen_nummern": ["3897","3912"]},
{"typ": "reihe","parzellen_nummern": ["4139","3951"]},
{"typ": "reihe","parzellen_nummern": ["4041","4042"]},
{"typ": "reihe","parzellen_nummern": ["1339","1338"]},
{"typ": "reihe","parzellen_nummern": ["1550","1319","1221"]},
{"typ": "reihe","parzellen_nummern": ["3335","1217"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Nutzungsziffer', '0.9');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["2405_2","2395"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Geschosszahl', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Nutzungsziffer', '6.0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Ausnuetzung_Art', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["1214_1","1214_2"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Gesamthoehe', '17');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Geschosshoehe', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Anteil_Arbeitsnutzung', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (45, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Uettligen: Szenario Basis
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Geschosszahl', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Nutzungsziffer', '0.3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["3965","3951"]},
{"typ": "reihe","parzellen_nummern": ["3966","3967"]},
{"typ": "reihe","parzellen_nummern": ["3968","3969"]},
{"typ": "reihe","parzellen_nummern": ["4278","4279"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Gesamthoehe', '7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Anteil_Arbeitsnutzung', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Nutzungsziffer', '0.4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5020","5888"]},
{"typ": "reihe","parzellen_nummern": ["5016","5131"]},
{"typ": "reihe","parzellen_nummern": ["5862","5863","5018"]},
{"typ": "reihe","parzellen_nummern": ["5426","3990"]},
{"typ": "reihe","parzellen_nummern": ["5871","5872"]},
{"typ": "reihe","parzellen_nummern": ["5797","5797"]},
{"typ": "reihe","parzellen_nummern": ["5794","5795"]},
{"typ": "reihe","parzellen_nummern": ["3874","5733"]},
{"typ": "reihe","parzellen_nummern": ["4936","4679"]},
{"typ": "reihe","parzellen_nummern": ["4454","4547","4546"]},
{"typ": "reihe","parzellen_nummern": ["4543","4542","4541","4455"]},
{"typ": "reihe","parzellen_nummern": ["4545","4544"]},
{"typ": "reihe","parzellen_nummern": ["4493","4492","4491","4490"]},
{"typ": "reihe","parzellen_nummern": ["4645","4644","4643","3863"]},
{"typ": "reihe","parzellen_nummern": ["2550","4646"]},
{"typ": "reihe","parzellen_nummern": ["4495","4494"]},
{"typ": "reihe","parzellen_nummern": ["4642","5143"]},
{"typ": "reihe","parzellen_nummern": ["4934","4935"]},
{"typ": "reihe","parzellen_nummern": ["4905","4906"]},
{"typ": "reihe","parzellen_nummern": ["4808","4992"]},
{"typ": "reihe","parzellen_nummern": ["4990","5163"]},
{"typ": "reihe","parzellen_nummern": ["5164","5165","5166"]},
{"typ": "reihe","parzellen_nummern": ["4956","4955","4954","2779"]},
{"typ": "reihe","parzellen_nummern": ["4383","4678","4677"]},
{"typ": "reihe","parzellen_nummern": ["4400","4401","4530","4531"]},
{"typ": "reihe","parzellen_nummern": ["6159","3572"]},
{"typ": "reihe","parzellen_nummern": ["5874","5875","5876","5877"]},
{"typ": "reihe","parzellen_nummern": ["5686","5156"]},
{"typ": "reihe","parzellen_nummern": ["2691","5141"]},
{"typ": "reihe","parzellen_nummern": ["5142","5157"]},
{"typ": "reihe","parzellen_nummern": ["4869","4466"]},
{"typ": "reihe","parzellen_nummern": ["4591","4467"]},
{"typ": "reihe","parzellen_nummern": ["2658","5127"]},
{"typ": "reihe","parzellen_nummern": ["5128","5040"]},
{"typ": "reihe","parzellen_nummern": ["2623","3308","2624"]},
{"typ": "reihe","parzellen_nummern": ["3587","3584"]},
{"typ": "reihe","parzellen_nummern": ["3586","3583"]},
{"typ": "reihe","parzellen_nummern": ["3585","3582"]},
{"typ": "reihe","parzellen_nummern": ["4409","4484"]},
{"typ": "reihe","parzellen_nummern": ["4485","4486"]},
{"typ": "reihe","parzellen_nummern": ["4487","4488"]},
{"typ": "reihe","parzellen_nummern": ["5043","5766","5767"]},
{"typ": "reihe","parzellen_nummern": ["4410","4750"]},
{"typ": "reihe","parzellen_nummern": ["2617","5893","5894","5895"]},
{"typ": "reihe","parzellen_nummern": ["5821","5822"]},
{"typ": "reihe","parzellen_nummern": ["5774","5817","5818","5819","5820"]},
{"typ": "reihe","parzellen_nummern": ["5925","5926"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Anteil_Arbeitsnutzung', '0.2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (51, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Uettligen: Szenario Basis + GF 0.7
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Geschosszahl', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Nutzungsziffer', '0.3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["3965","3951"]},
{"typ": "reihe","parzellen_nummern": ["3966","3967"]},
{"typ": "reihe","parzellen_nummern": ["3968","3969"]},
{"typ": "reihe","parzellen_nummern": ["4278","4279"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Gesamthoehe', '7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Anteil_Arbeitsnutzung', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Nutzungsziffer', '0.4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5020","5888"]},
{"typ": "reihe","parzellen_nummern": ["5016","5131"]},
{"typ": "reihe","parzellen_nummern": ["5862","5863","5018"]},
{"typ": "reihe","parzellen_nummern": ["5426","3990"]},
{"typ": "reihe","parzellen_nummern": ["5871","5872"]},
{"typ": "reihe","parzellen_nummern": ["5797","5797"]},
{"typ": "reihe","parzellen_nummern": ["5794","5795"]},
{"typ": "reihe","parzellen_nummern": ["3874","5733"]},
{"typ": "reihe","parzellen_nummern": ["4936","4679"]},
{"typ": "reihe","parzellen_nummern": ["4454","4547","4546"]},
{"typ": "reihe","parzellen_nummern": ["4543","4542","4541","4455"]},
{"typ": "reihe","parzellen_nummern": ["4545","4544"]},
{"typ": "reihe","parzellen_nummern": ["4493","4492","4491","4490"]},
{"typ": "reihe","parzellen_nummern": ["4645","4644","4643","3863"]},
{"typ": "reihe","parzellen_nummern": ["2550","4646"]},
{"typ": "reihe","parzellen_nummern": ["4495","4494"]},
{"typ": "reihe","parzellen_nummern": ["4642","5143"]},
{"typ": "reihe","parzellen_nummern": ["4934","4935"]},
{"typ": "reihe","parzellen_nummern": ["4905","4906"]},
{"typ": "reihe","parzellen_nummern": ["4808","4992"]},
{"typ": "reihe","parzellen_nummern": ["4990","5163"]},
{"typ": "reihe","parzellen_nummern": ["5164","5165","5166"]},
{"typ": "reihe","parzellen_nummern": ["4956","4955","4954","2779"]},
{"typ": "reihe","parzellen_nummern": ["4383","4678","4677"]},
{"typ": "reihe","parzellen_nummern": ["4400","4401","4530","4531"]},
{"typ": "reihe","parzellen_nummern": ["6159","3572"]},
{"typ": "reihe","parzellen_nummern": ["5874","5875","5876","5877"]},
{"typ": "reihe","parzellen_nummern": ["5686","5156"]},
{"typ": "reihe","parzellen_nummern": ["2691","5141"]},
{"typ": "reihe","parzellen_nummern": ["5142","5157"]},
{"typ": "reihe","parzellen_nummern": ["4869","4466"]},
{"typ": "reihe","parzellen_nummern": ["4591","4467"]},
{"typ": "reihe","parzellen_nummern": ["2658","5127"]},
{"typ": "reihe","parzellen_nummern": ["5128","5040"]},
{"typ": "reihe","parzellen_nummern": ["2623","3308","2624"]},
{"typ": "reihe","parzellen_nummern": ["3587","3584"]},
{"typ": "reihe","parzellen_nummern": ["3586","3583"]},
{"typ": "reihe","parzellen_nummern": ["3585","3582"]},
{"typ": "reihe","parzellen_nummern": ["4409","4484"]},
{"typ": "reihe","parzellen_nummern": ["4485","4486"]},
{"typ": "reihe","parzellen_nummern": ["4487","4488"]},
{"typ": "reihe","parzellen_nummern": ["5043","5766","5767"]},
{"typ": "reihe","parzellen_nummern": ["4410","4750"]},
{"typ": "reihe","parzellen_nummern": ["2617","5893","5894","5895"]},
{"typ": "reihe","parzellen_nummern": ["5821","5822"]},
{"typ": "reihe","parzellen_nummern": ["5774","5817","5818","5819","5820"]},
{"typ": "reihe","parzellen_nummern": ["5172_1","5172_2"]},
{"typ": "reihe","parzellen_nummern": ["6090_1","6090_2"]},
{"typ": "reihe","parzellen_nummern": ["5925","5926"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Freiflaechenziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Anteil_Arbeitsnutzung', '0.2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (52, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Uettligen: Szenario Basis Keine Ausnutzung
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Geschosszahl', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["3965","3951"]},
{"typ": "reihe","parzellen_nummern": ["3966","3967"]},
{"typ": "reihe","parzellen_nummern": ["3968","3969"]},
{"typ": "reihe","parzellen_nummern": ["4278","4279"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Gesamthoehe', '7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Anteil_Arbeitsnutzung', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5020","5888"]},
{"typ": "reihe","parzellen_nummern": ["5016","5131"]},
{"typ": "reihe","parzellen_nummern": ["5862","5863","5018"]},
{"typ": "reihe","parzellen_nummern": ["5426","3990"]},
{"typ": "reihe","parzellen_nummern": ["5871","5872"]},
{"typ": "reihe","parzellen_nummern": ["5797","5797"]},
{"typ": "reihe","parzellen_nummern": ["5794","5795"]},
{"typ": "reihe","parzellen_nummern": ["3874","5733"]},
{"typ": "reihe","parzellen_nummern": ["4936","4679"]},
{"typ": "reihe","parzellen_nummern": ["4454","4547","4546"]},
{"typ": "reihe","parzellen_nummern": ["4543","4542","4541","4455"]},
{"typ": "reihe","parzellen_nummern": ["4545","4544"]},
{"typ": "reihe","parzellen_nummern": ["4493","4492","4491","4490"]},
{"typ": "reihe","parzellen_nummern": ["4645","4644","4643","3863"]},
{"typ": "reihe","parzellen_nummern": ["2550","4646"]},
{"typ": "reihe","parzellen_nummern": ["4495","4494"]},
{"typ": "reihe","parzellen_nummern": ["4642","5143"]},
{"typ": "reihe","parzellen_nummern": ["4934","4935"]},
{"typ": "reihe","parzellen_nummern": ["4905","4906"]},
{"typ": "reihe","parzellen_nummern": ["4808","4992"]},
{"typ": "reihe","parzellen_nummern": ["4990","5163"]},
{"typ": "reihe","parzellen_nummern": ["5164","5165","5166"]},
{"typ": "reihe","parzellen_nummern": ["4956","4955","4954","2779"]},
{"typ": "reihe","parzellen_nummern": ["4383","4678","4677"]},
{"typ": "reihe","parzellen_nummern": ["4400","4401","4530","4531"]},
{"typ": "reihe","parzellen_nummern": ["6159","3572"]},
{"typ": "reihe","parzellen_nummern": ["5874","5875","5876","5877"]},
{"typ": "reihe","parzellen_nummern": ["5686","5156"]},
{"typ": "reihe","parzellen_nummern": ["2691","5141"]},
{"typ": "reihe","parzellen_nummern": ["5142","5157"]},
{"typ": "reihe","parzellen_nummern": ["4869","4466"]},
{"typ": "reihe","parzellen_nummern": ["4591","4467"]},
{"typ": "reihe","parzellen_nummern": ["2658","5127"]},
{"typ": "reihe","parzellen_nummern": ["5128","5040"]},
{"typ": "reihe","parzellen_nummern": ["2623","3308","2624"]},
{"typ": "reihe","parzellen_nummern": ["3587","3584"]},
{"typ": "reihe","parzellen_nummern": ["3586","3583"]},
{"typ": "reihe","parzellen_nummern": ["3585","3582"]},
{"typ": "reihe","parzellen_nummern": ["4409","4484"]},
{"typ": "reihe","parzellen_nummern": ["4485","4486"]},
{"typ": "reihe","parzellen_nummern": ["4487","4488"]},
{"typ": "reihe","parzellen_nummern": ["5043","5766","5767"]},
{"typ": "reihe","parzellen_nummern": ["4410","4750"]},
{"typ": "reihe","parzellen_nummern": ["2617","5893","5894","5895"]},
{"typ": "reihe","parzellen_nummern": ["5821","5822"]},
{"typ": "reihe","parzellen_nummern": ["5774","5817","5818","5819","5820"]},
{"typ": "reihe","parzellen_nummern": ["5172_1","5172_2"]},
{"typ": "reihe","parzellen_nummern": ["6090_1","6090_2"]},
{"typ": "reihe","parzellen_nummern": ["5925","5926"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Geschosszahl', '2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Gesamthoehe', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Nutzungsziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Ausnuetzung_Art', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Anteil_Arbeitsnutzung', '0.2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (53, 'Z5', 'Platzbedarf_Wohnnutzung', '60');


--Uettligen: Szenario Basis + W3
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Nutzungsziffer', '0.5');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["3965","3951"]},
{"typ": "reihe","parzellen_nummern": ["3966","3967"]},
{"typ": "reihe","parzellen_nummern": ["3968","3969"]},
{"typ": "reihe","parzellen_nummern": ["4278","4279"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Anteil_Arbeitsnutzung', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z1', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Nutzungsziffer', '0.6');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Zonenparameter', '{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5020","5888"]},
{"typ": "reihe","parzellen_nummern": ["5016","5131"]},
{"typ": "reihe","parzellen_nummern": ["5862","5863","5018"]},
{"typ": "reihe","parzellen_nummern": ["5426","3990"]},
{"typ": "reihe","parzellen_nummern": ["5871","5872"]},
{"typ": "reihe","parzellen_nummern": ["5797","5797"]},
{"typ": "reihe","parzellen_nummern": ["5794","5795"]},
{"typ": "reihe","parzellen_nummern": ["3874","5733"]},
{"typ": "reihe","parzellen_nummern": ["4936","4679"]},
{"typ": "reihe","parzellen_nummern": ["4454","4547","4546"]},
{"typ": "reihe","parzellen_nummern": ["4543","4542","4541","4455"]},
{"typ": "reihe","parzellen_nummern": ["4545","4544"]},
{"typ": "reihe","parzellen_nummern": ["4493","4492","4491","4490"]},
{"typ": "reihe","parzellen_nummern": ["4645","4644","4643","3863"]},
{"typ": "reihe","parzellen_nummern": ["2550","4646"]},
{"typ": "reihe","parzellen_nummern": ["4495","4494"]},
{"typ": "reihe","parzellen_nummern": ["4642","5143"]},
{"typ": "reihe","parzellen_nummern": ["4934","4935"]},
{"typ": "reihe","parzellen_nummern": ["4905","4906"]},
{"typ": "reihe","parzellen_nummern": ["4808","4992"]},
{"typ": "reihe","parzellen_nummern": ["4990","5163"]},
{"typ": "reihe","parzellen_nummern": ["5164","5165","5166"]},
{"typ": "reihe","parzellen_nummern": ["4956","4955","4954","2779"]},
{"typ": "reihe","parzellen_nummern": ["4383","4678","4677"]},
{"typ": "reihe","parzellen_nummern": ["4400","4401","4530","4531"]},
{"typ": "reihe","parzellen_nummern": ["6159","3572"]},
{"typ": "reihe","parzellen_nummern": ["5874","5875","5876","5877"]},
{"typ": "reihe","parzellen_nummern": ["5686","5156"]},
{"typ": "reihe","parzellen_nummern": ["2691","5141"]},
{"typ": "reihe","parzellen_nummern": ["5142","5157"]},
{"typ": "reihe","parzellen_nummern": ["4869","4466"]},
{"typ": "reihe","parzellen_nummern": ["4591","4467"]},
{"typ": "reihe","parzellen_nummern": ["2658","5127"]},
{"typ": "reihe","parzellen_nummern": ["5128","5040"]},
{"typ": "reihe","parzellen_nummern": ["2623","3308","2624"]},
{"typ": "reihe","parzellen_nummern": ["3587","3584"]},
{"typ": "reihe","parzellen_nummern": ["3586","3583"]},
{"typ": "reihe","parzellen_nummern": ["3585","3582"]},
{"typ": "reihe","parzellen_nummern": ["4409","4484"]},
{"typ": "reihe","parzellen_nummern": ["4485","4486"]},
{"typ": "reihe","parzellen_nummern": ["4487","4488"]},
{"typ": "reihe","parzellen_nummern": ["5043","5766","5767"]},
{"typ": "reihe","parzellen_nummern": ["4410","4750"]},
{"typ": "reihe","parzellen_nummern": ["2617","5893","5894","5895"]},
{"typ": "reihe","parzellen_nummern": ["5821","5822"]},
{"typ": "reihe","parzellen_nummern": ["5774","5817","5818","5819","5820"]},
{"typ": "reihe","parzellen_nummern": ["5172_1","5172_2"]},
{"typ": "reihe","parzellen_nummern": ["6090_1","6090_2"]},
{"typ": "reihe","parzellen_nummern": ["5925","5926"]}
  ]
}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Anteil_Arbeitsnutzung', '0.2');                                       
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z2', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Anteil_Arbeitsnutzung', '0.1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z3', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Anteil_Arbeitsnutzung', '0.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z4', 'Platzbedarf_Wohnnutzung', '60');

INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Grosser_Grenzabstand', '10');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Kleiner_Grenzabstand', '4');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Geschosszahl', '3');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Geschosshoehe', '2.8');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Nutzungsziffer', '0.7');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Ausnuetzung_Art', '1');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Zonenparameter', '{}');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Freiflaechenziffer', '0');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Gesamthoehe', '13');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Anteil_Arbeitsnutzung', '0.2');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Platzbedarf_Arbeitsnutzung', '80');
INSERT INTO dencity.szenario_parameter (sid, zone_code, parameter_name, parameter_wert) VALUES (54, 'Z5', 'Platzbedarf_Wohnnutzung', '60');

--INSERT INTO dencity.abstandslinie (sid,gestaltend,geom) (SELECT 25, gestaltend, geom FROM dencity.abstandslinie WHERE gid IN (15,16,17,18));

INSERT INTO dencity.abstandslinie (sid,gestaltend,geom) VALUES (45, false, ST_SetSRID(ST_GeomFromText('LineString (2625974.69446981977671385 1228553.01842663693241775, 2625975.50110236974433064 1228553.45055121718905866, 2625994.23610361991450191 1228563.33179995440877974, 2626016.21684060897678137 1228574.98956085648387671, 2626019.7698649363592267 1228576.86210070480592549, 2626036.75716233067214489 1228585.85989474575035274, 2626042.30756249697878957 1228588.82715019793249667, 2626047.43544085090979934 1228592.10169424046762288, 2626053.51399328140541911 1228596.6342009506188333, 2626054.9832168547436595 1228598.10342452395707369, 2626056.17396014276891947 1228599.73589516105130315, 2626057.47033388447016478 1228602.49188970727846026, 2626058.41140519315376878 1228604.20118249184451997, 2626058.84352977341040969 1228605.3823230117559433, 2626059.15081836376339197 1228606.61147737363353372, 2626059.15081836329773068 1228608.9545528762973845, 2626058.27696643397212029 1228613.89037586143240333, 2626057.11503145098686218 1228619.19110404746606946, 2626055.48256081389263272 1228626.02827518619596958, 2626053.87889848276972771 1228632.87504909350536764, 2626052.61133304703980684 1228637.39795303507708013, 2626049.69209143659099936 1228647.99940940737724304, 2626044.24732172396034002 1228667.84833180019631982, 2626042.02908221120014787 1228668.87582802469842136, 2626041.31847734795883298 1228670.84439555904828012)'),2056));
INSERT INTO dencity.abstandslinie (sid,gestaltend,geom) VALUES (45, false, ST_SetSRID(ST_GeomFromText('LineString (2626051.09136141883209348 1228697.50763490027748048, 2626053.08753691241145134 1228690.34156892099417746, 2626057.93126676883548498 1228673.03382676444016397, 2626064.67241022316738963 1228648.91167240962386131, 2626066.72740267217159271 1228641.44071855279617012, 2626071.4807730563916266 1228624.40540731674991548, 2626072.83476340863853693 1228620.40105287171900272, 2626074.37120636133477092 1228616.80961747001856565, 2626076.2821572832763195 1228613.34301805845461786, 2626076.84872062131762505 1228612.6516187300439924, 2626077.50170887634158134 1228612.0658498543780297, 2626078.19310820521786809 1228611.5953142000362277, 2626078.89411030197516084 1228611.29762837802991271, 2626079.73915392579510808 1228611.00954532437026501, 2626080.55538924457505345 1228610.87510656611993909, 2626081.26599411014467478 1228610.88470933446660638, 2626082.22627095552161336 1228610.98073701909743249, 2626082.83124536788091063 1228611.18239515647292137, 2626091.34890098590403795 1228615.91656000399962068, 2626093.75919586746022105 1228617.34737250348553061, 2626096.51519041368737817 1228618.97024037223309278, 2626102.87222312996163964 1228623.11863634409382939, 2626123.22048948192968965 1228637.97411914146505296, 2626135.41600541770458221 1228646.85667996085248888, 2626142.84854820044711232 1228652.21502475789748132, 2626145.94063964206725359 1228654.46207257593050599, 2626147.13138293055817485 1228655.28791066282428801, 2626148.29331791307777166 1228656.04652937082573771, 2626148.91749786259606481 1228656.4594484141562134, 2626150.61565176630392671 1228657.63311311788856983)'),2056));
INSERT INTO dencity.abstandslinie (sid,gestaltend,geom) VALUES (45, false, ST_SetSRID(ST_GeomFromText('LineString (2626159.03423941787332296 1228642.10458489833399653, 2626158.49408369278535247 1228641.46360010467469692, 2626157.43537847092375159 1228640.61375509691424668, 2626156.2542379517108202 1228639.79271839442662895, 2626150.44216234842315316 1228636.14126569195650518, 2626141.65562921715900302 1228630.27877555415034294, 2626132.00484692584723234 1228623.40079265250824392, 2626107.65102579351514578 1228605.65847759647294879, 2626106.67874548817053437 1228604.94907307741232216, 2626106.08457419043406844 1228604.64298483310267329, 2626105.38237174740061164 1228604.57456510793417692, 2626104.73058383911848068 1228604.72940974915400147, 2626103.44861425133422017 1228605.25516132172197104, 2626090.39124985178932548 1228595.31989701511338353, 2626086.99186982028186321 1228592.28782287705689669, 2626086.23925284342840314 1228591.41277060215361416, 2626084.46754206484183669 1228589.40339130442589521, 2626082.28531293477863073 1228586.23447771626524627, 2626081.96842157607898116 1228585.56828565523028374, 2626081.79917278233915567 1228585.08214550232514739, 2626081.70554579002782702 1228584.52758562448434532, 2626081.7307530571706593 1228583.94421744113788009, 2626081.82077901111915708 1228583.23841396067291498, 2626085.0869206297211349 1228570.88325200485996902, 2626087.61304890364408493 1228561.78162803431041539, 2626090.58030435396358371 1228551.3242131934966892, 2626092.9227796820923686 1228542.92119062738493085, 2626095.86482786573469639 1228532.30533010745421052, 2626099.03374145366251469 1228520.92604949558153749, 2626105.31035098107531667 1228498.22150384378619492, 2626106.02335653826594353 1228496.29494842374697328, 2626106.39066243171691895 1228495.04898921749554574, 2626106.68234652327373624 1228494.02269333973526955)'),2056));
INSERT INTO dencity.abstandslinie (sid,gestaltend,geom) VALUES (45, false, ST_SetSRID(ST_GeomFromText('LineString (2626090.32823168393224478 1228496.18871779763139784, 2626084.61338411131873727 1228516.92709660949185491, 2626081.49578531691804528 1228528.0957164887804538, 2626078.37098444672301412 1228539.28234155871905386, 2626072.18980243057012558 1228561.29188684374094009, 2626068.55095336260274053 1228574.7246594715397805, 2626068.03060334734618664 1228575.61771693686023355, 2626067.32840090431272984 1228576.52517855539917946, 2626066.48395745363086462 1228577.30840435693971813, 2626065.56209168257191777 1228577.96019226522184908, 2626064.51599009474739432 1228578.47334020468406379, 2626063.44288072036579251 1228578.82984298327937722, 2626062.41658484237268567 1228579.00629385351203382, 2626061.40469311689957976 1228579.06030942592769861, 2626060.3819982772693038 1228578.96668243361636996, 2626059.48533977335318923 1228578.76862533437088132, 2626056.10936649097129703 1228576.96630573109723628, 2626052.89543992513790727 1228575.28732168511487544, 2626043.58855677954852581 1228570.38270770059898496, 2626041.12904770998284221 1228569.07012928836047649, 2626025.18102496257051826 1228560.56140125845558941, 2626014.697202502284199 1228554.97499071038328111, 2626002.43326684040948749 1228548.37188705196604133, 2626001.53780868230387568 1228548.13781957072205842, 2625990.34518187586218119 1228542.23406753339804709, 2625973.01638602698221803 1228533.09028140315786004, 2625969.33132363576442003 1228531.16252563521265984)'),2056));





