WITH params (pid, minSid, maxSid) as (
     values (4, 594, 1025)
)
SELECT '{ "data": ' || array_to_json(array_agg(b)) || ' }'
FROM (
  SELECT row_to_json(t) AS szenario
  FROM (
    SELECT id AS sid, titel AS titel, 
    (
      SELECT array_to_json(array_agg(row_to_json(w)))
        FROM (
          SELECT parameter_name, parameter_wert, parameter_wert_formatted, parameter_label
            FROM (
              SELECT 
                  sid
                  ,parameter_name
                  ,parameter_wert
                  ,CASE 
                    WHEN parameter_name = 'Grosser_Grenzabstand' THEN parameter_wert || ' m'
                    WHEN parameter_name = 'Kleiner_Grenzabstand' THEN parameter_wert || ' m'
                    WHEN parameter_name = 'Geschosszahl' THEN parameter_wert
                    WHEN parameter_name = 'Geschosshoehe' THEN parameter_wert || ' m'
                    WHEN parameter_name = 'Nutzungsziffer' THEN parameter_wert
                    WHEN parameter_name = 'Ausnuetzung_Art' THEN (
                      CASE WHEN parameter_wert = '0' THEN 'Keine'
                        WHEN parameter_wert = '1' THEN 'GFZ'
                        WHEN parameter_wert = '2' THEN 'BMZ'
                        WHEN parameter_wert = '3' THEN 'ÜZ'
                        ELSE ''
                      END
                      )
                    WHEN parameter_name = 'Zonenparameter' THEN parameter_wert
                    WHEN parameter_name = 'Gruenflaechenziffer' THEN (
                      CASE 
                        WHEN parameter_wert = '0' THEN 'keine'
                        ELSE parameter_wert
                      END
                      )
                    WHEN parameter_name = 'Gesamthoehe' THEN (
                      CASE 
                        WHEN parameter_wert = '0' THEN 'keine'
                        ELSE parameter_wert || ' m'
                      END
                      )
                    WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN parameter_wert
                    WHEN parameter_name = 'Platzbedarf_Arbeitsnutzung' THEN parameter_wert || ' m² pro Beschäftigtem'
                    WHEN parameter_name = 'Platzbedarf_Wohnnutzung' THEN parameter_wert || ' m² pro Einwohner'
                    ELSE ''
                  END AS parameter_wert_formatted
                  ,CASE 
                    WHEN parameter_name = 'Grosser_Grenzabstand' THEN 'Grosser Grenzabstand'
                    WHEN parameter_name = 'Kleiner_Grenzabstand' THEN 'Kleiner Grenzabstand'
                    WHEN parameter_name = 'Geschosszahl' THEN 'Geschosszahl'
                    WHEN parameter_name = 'Geschosshoehe' THEN 'Geschosshöehe'
                    WHEN parameter_name = 'Nutzungsziffer' THEN 'Nutzungsziffer'
                    WHEN parameter_name = 'Ausnuetzung_Art' THEN 'Art der Ausnützung'
                    WHEN parameter_name = 'Zonenparameter' THEN 'Zonenparameter'
                    WHEN parameter_name = 'Gruenflaechenziffer' THEN 'Gruenfläechenziffer'
                    WHEN parameter_name = 'Gesamthoehe' THEN 'Gesamthöhe'
                    WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN 'Anteil Arbeitsnutzung'
                    WHEN parameter_name = 'Platzbedarf_Arbeitsnutzung' THEN 'Platzbedarf Arbeitsnutzung'
                    WHEN parameter_name = 'Platzbedarf_Wohnnutzung' THEN 'Platzbedarf Wohnnutzung'
                    ELSE ''
                  END AS parameter_label
                  ,CASE 
                    WHEN parameter_name = 'Grosser_Grenzabstand' THEN 0
                    WHEN parameter_name = 'Kleiner_Grenzabstand' THEN 1
                    WHEN parameter_name = 'Geschosszahl' THEN 2
                    WHEN parameter_name = 'Geschosshoehe' THEN 3
                    WHEN parameter_name = 'Nutzungsziffer' THEN 5
                    WHEN parameter_name = 'Ausnuetzung_Art' THEN 4
                    WHEN parameter_name = 'Zonenparameter' THEN 10
                    WHEN parameter_name = 'Gruenflaechenziffer' THEN 5
                    WHEN parameter_name = 'Gesamthoehe' THEN 6
                    WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN 7
                    WHEN parameter_name = 'Platzbedarf_Arbeitsnutzung' THEN 8
                    WHEN parameter_name = 'Platzbedarf_Wohnnutzung' THEN 9
                    ELSE 100
                  END AS parameter_order
                FROM dencity.szenario_parameter
                WHERE zone_code = 'Z1' AND szenario_parameter.sid = szenario.id  
              UNION ALL
              SELECT 
                  sz.id AS sid
                  ,CASE
                    WHEN n = 10 THEN 'anrechenbare_grundstuecksflaeche'
                    WHEN n = 11 THEN 'geschossflaeche'
                    WHEN n = 12 THEN 'volumen'
                    WHEN n = 13 THEN 'nutzer_wohnen'
                    WHEN n = 14 THEN 'nutzer_arbeit'
                    WHEN n = 15 THEN 'grundflaeche'
                    WHEN n = 16 THEN 'bmz' 
                    WHEN n = 17 THEN 'gfz' 
                    WHEN n = 18 THEN 'uz'
                    ELSE ''
                  END AS parameter_name
                  ,CASE
                    WHEN n = 10 THEN CAST(anrechenbare_grundstuecksflaeche AS VARCHAR)
                    WHEN n = 11 THEN CAST(geschossflaeche AS VARCHAR)
                    WHEN n = 12 THEN CAST(Volumen AS VARCHAR)
                    WHEN n = 13 THEN CAST(nutzer_wohnen AS VARCHAR)
                    WHEN n = 14 THEN CAST(nutzer_arbeit AS VARCHAR)
                    WHEN n = 15 THEN CAST(Grundflaeche AS VARCHAR)
                    WHEN n = 16 THEN CAST(Volumen/anrechenbare_grundstuecksflaeche AS VARCHAR) 
                    WHEN n = 17 THEN CAST(geschossflaeche/anrechenbare_grundstuecksflaeche AS VARCHAR) 
                    WHEN n = 18 THEN CAST(Grundflaeche/anrechenbare_grundstuecksflaeche AS VARCHAR)
                    ELSE ''
                  END AS parameter_wert
                  ,CASE
                    WHEN n = 10 THEN CAST(ROUND(anrechenbare_grundstuecksflaeche) AS VARCHAR) || ' m²'
                    WHEN n = 11 THEN CAST(ROUND(geschossflaeche) AS VARCHAR) || ' m²'
                    WHEN n = 12 THEN CAST(ROUND(Volumen) AS VARCHAR) || ' m³'
                    WHEN n = 13 THEN CAST(ROUND(nutzer_wohnen) AS VARCHAR)
                    WHEN n = 14 THEN CAST(ROUND(nutzer_arbeit) AS VARCHAR)
                    WHEN n = 15 THEN CAST(ROUND(Grundflaeche) AS VARCHAR) || ' m²' 
                    WHEN n = 16 THEN CAST(ROUND(CAST(Volumen/anrechenbare_grundstuecksflaeche AS NUMERIC),2) AS VARCHAR) 
                    WHEN n = 17 THEN CAST(ROUND(CAST(geschossflaeche/anrechenbare_grundstuecksflaeche AS NUMERIC),2) AS VARCHAR) 
                    WHEN n = 18 THEN CAST(ROUND(CAST(Grundflaeche/anrechenbare_grundstuecksflaeche AS NUMERIC),2) AS VARCHAR)
                    ELSE ''
                  END AS parameter_wert_formatted
                  ,CASE
                    WHEN n = 10 THEN 'Anrechenbare Grundstücksfläche'
                    WHEN n = 11 THEN 'Geschossfläche'
                    WHEN n = 12 THEN 'Bauvolumen'
                    WHEN n = 13 THEN 'Einwohner'
                    WHEN n = 14 THEN 'Beschäftigte'
                    WHEN n = 15 THEN 'Gebäude Grundfläche'
                    WHEN n = 16 THEN 'BMZ' 
                    WHEN n = 17 THEN 'GFZ' 
                    WHEN n = 18 THEN 'ÜZ'
                    ELSE ''
                  END AS parameter_label
                  ,n AS parameter_order    
                FROM dencity.szenario AS sz
                LEFT JOIN (
                  SELECT sid, SUM(hoehe*ST_Area(geom)) AS Volumen, SUM(geschossflaeche) AS geschossflaeche, SUM(nutzer_wohnen) AS nutzer_wohnen, 
                     SUM(nutzer_arbeit) AS nutzer_arbeit, SUM(ST_Area(geom)) AS Grundflaeche
                    FROM dencity.gebaeude
                    GROUP BY sid) AS gebaeude ON sz.id = gebaeude.sid
                LEFT JOIN  (
                  SELECT pid, SUM(anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche
                    FROM dencity.parzelle
                    GROUP BY pid
                ) AS parzelle ON sz.pid = parzelle.pid
                CROSS JOIN (SELECT n FROM unnest( ARRAY[10,11,12,13,14,15,16,17,18] ) AS n) AS pivot
				WHERE sz.id = szenario.id
            ) AS u
        ) AS w
    ) AS werte
    FROM dencity.szenario,params
    WHERE szenario.pid = params.pid AND szenario.id BETWEEN params.minSid AND params.maxSid
  ) AS t
) AS b
