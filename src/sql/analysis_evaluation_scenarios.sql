﻿---
--- Global Query
---
--Global
SELECT 
    szenario.titel
    ,CASE WHEN szenario.pid = 4 THEN 'Langenthal' ELSE 'Uettligen' END AS Projekt
    ,kg.anrechenbare_grundstuecksflaeche
    ,kg.area_gebaeude
    ,kg.volumen
    ,kg.geschossflaeche
    ,kg.gfz
    ,ki.avgGFZ
    ,kg.bmz 
    ,ki.avgBMZ
    ,kg.uz
    ,ki.avgUZ
    ,ki.minUZ
    ,ki.minGFZ
    ,ki.minBMZ
    ,ki.maxUZ
    ,ki.maxGFZ
    ,ki.maxBMZ
    ,kg.area_parzelle
    ,kg.sid
    ,szenario.pid
FROM
(SELECT 
    szenario.id AS sid
    ,anrechenbare_grundstuecksflaeche
    ,area_parzelle
    ,area_gebaeude
    ,geschossflaeche
    ,volumen
    ,COALESCE(volumen,0)/anrechenbare_grundstuecksflaeche AS bmz
    ,COALESCE(geschossflaeche,0)/anrechenbare_grundstuecksflaeche AS gfz
    ,COALESCE(area_gebaeude,0)/anrechenbare_grundstuecksflaeche AS uz
  FROM dencity.szenario
  LEFT JOIN 
    (SELECT
         pid
         ,SUM(parzelle.anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche
         ,SUM(ST_Area(parzelle.geom)) AS area_parzelle 
       FROM dencity.parzelle 
       GROUP BY pid) AS pc ON szenario.pid = pc.pid
  LEFT JOIN 
    (SELECT 
         sid
         ,SUM(ST_Area(gebaeude.geom)) AS area_gebaeude
         ,SUM(gebaeude.geschossflaeche) AS geschossflaeche
         ,SUM(ST_Area(gebaeude.geom)*gebaeude.hoehe) AS volumen
       FROM dencity.gebaeude
       GROUP BY sid) AS gb ON gb.sid = szenario.id
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39)
  AS kg
LEFT JOIN   
--Individual
(SELECT 
sid,
AVG(uz) AS avgUZ,
AVG(gfz) AS avgGFZ,
AVG(bmz) AS avgBMZ,
MIN(uz) AS minUZ,
MIN(gfz) AS minGFZ,
MIN(bmz) AS minBMZ,
MAX(uz) AS maxUZ,
MAX(gfz) AS maxGFZ,
MAX(bmz) AS maxBMZ
FROM
(SELECT sid, pgid,
    SUM(gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS uz,
    SUM(geschosszahl*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS gfz,
    SUM(hoehe*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS bmz
FROM
(
SELECT 
    szenario.id AS sid,
    parzelle.gid AS pgid,
    parzelle.nummer,
    parzelle.anrechenbare_grundstuecksflaeche,
    ST_Area(parzelle.geom) AS parzelle_area,
    COALESCE(gebaeude.hoehe,0) AS hoehe,
    COALESCE(gebaeude.geschosszahl,0) AS geschosszahl,
    gebaeude.geschossflaeche,
    gebaeude.nutzer_wohnen,
    gebaeude.nutzer_arbeit,
    COALESCE(ST_Area(ST_Intersection(gebaeude.geom,parzelle.geom)),0) gebaeude_area,
    gebaeude.anteil_arbeitsnutzung
  FROM dencity.szenario
  LEFT JOIN dencity.parzelle ON szenario.pid = parzelle.pid
  LEFT JOIN dencity.gebaeude ON gebaeude.sid = szenario.id AND ST_Intersects(parzelle.geom, gebaeude.geom)
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39
) AS i1
GROUP BY sid, pgid
) AS i2
GROUP BY sid) AS ki ON kg.sid = ki.sid

  LEFT JOIN dencity.szenario ON kg.sid = szenario.id
  
  ORDER BY sid








---
--- By Zone
---
--Global
SELECT kg.sid
    ,szenario.titel
    ,CASE WHEN szenario.pid = 4 THEN 'Langenthal' ELSE 'Uettligen' END AS Projekt
    ,szenario.pid
    ,ki.zone_code
    ,kg.anrechenbare_grundstuecksflaeche
    ,kg.area_parzelle
    ,kg.area_gebaeude
    ,kg.volumen
    ,kg.geschossflaeche
    ,kg.bmz 
    ,kg.gfz
    ,kg.uz

    ,ki.avgUZ
    ,ki.avgGFZ
    ,ki.avgBMZ
    ,ki.minUZ
    ,ki.minGFZ
    ,ki.minBMZ
    ,ki.maxUZ
    ,ki.maxGFZ
    ,ki.maxBMZ
    ,pv.Grosser_Grenzabstand
    ,pv.Kleiner_Grenzabstand
    ,pv.Geschosszahl    
    ,pv.Geschosshoehe
    ,pv.Nutzungsziffer    
    ,pv.Ausnuetzung_Art    
    ,pv.Ausnuetzung_Art_Label    
    ,pv.Gruenflaechenziffer    
    ,pv.Gesamthoehe    
    ,pv.Anteil_Arbeitsnutzung
    ,pv.Anteil_Wohnnutzung    
    ,pv.Platzbedarf_Arbeitsnutzung    
    ,pv.Platzbedarf_Wohnnutzung
FROM
(SELECT 
    szenario.id AS sid
    ,anrechenbare_grundstuecksflaeche
    ,area_parzelle
    ,area_gebaeude
    ,geschossflaeche
    ,volumen
    ,COALESCE(volumen,0)/anrechenbare_grundstuecksflaeche AS bmz
    ,COALESCE(geschossflaeche,0)/anrechenbare_grundstuecksflaeche AS gfz
    ,COALESCE(area_gebaeude,0)/anrechenbare_grundstuecksflaeche AS uz
  FROM dencity.szenario
  LEFT JOIN 
    (SELECT
         pid
         ,SUM(parzelle.anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche
         ,SUM(ST_Area(parzelle.geom)) AS area_parzelle 
       FROM dencity.parzelle 
       GROUP BY pid) AS pc ON szenario.pid = pc.pid
  LEFT JOIN 
    (SELECT 
         sid
         ,SUM(ST_Area(gebaeude.geom)) AS area_gebaeude
         ,SUM(gebaeude.geschossflaeche) AS geschossflaeche
         ,SUM(ST_Area(gebaeude.geom)*gebaeude.hoehe) AS volumen
       FROM dencity.gebaeude
       GROUP BY sid) AS gb ON gb.sid = szenario.id
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39)
  AS kg
LEFT JOIN   
--Individual
(SELECT 
sid,
zone_code,
AVG(uz) AS avgUZ,
AVG(gfz) AS avgGFZ,
AVG(bmz) AS avgBMZ,
MIN(uz) AS minUZ,
MIN(gfz) AS minGFZ,
MIN(bmz) AS minBMZ,
MAX(uz) AS maxUZ,
MAX(gfz) AS maxGFZ,
MAX(bmz) AS maxBMZ
FROM
(SELECT sid, pgid, zone_code,
    SUM(gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS uz,
    SUM(geschosszahl*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS gfz,
    SUM(hoehe*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS bmz
FROM
(
SELECT 
    szenario.id AS sid,
    parzelle.gid AS pgid,
    parzelle.nummer,
    parzelle.anrechenbare_grundstuecksflaeche,
	parzelle.zone_code,
    ST_Area(parzelle.geom) AS parzelle_area,
    COALESCE(gebaeude.hoehe,0) AS hoehe,
    COALESCE(gebaeude.geschosszahl,0) AS geschosszahl,
    gebaeude.geschossflaeche,
    gebaeude.nutzer_wohnen,
    gebaeude.nutzer_arbeit,
    COALESCE(ST_Area(ST_Intersection(gebaeude.geom,parzelle.geom)),0) gebaeude_area,
    gebaeude.anteil_arbeitsnutzung
  FROM dencity.szenario
  LEFT JOIN dencity.parzelle ON szenario.pid = parzelle.pid
  LEFT JOIN dencity.gebaeude ON gebaeude.sid = szenario.id AND ST_Intersects(parzelle.geom, gebaeude.geom)
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39
) AS i1
GROUP BY sid, pgid, zone_code
) AS i2
GROUP BY sid, zone_code) AS ki ON kg.sid = ki.sid



LEFT JOIN
--Parameter
  (SELECT sid, 
      zone_code,
      MAX(Grosser_Grenzabstand) AS Grosser_Grenzabstand,
      MAX(Kleiner_Grenzabstand) AS Kleiner_Grenzabstand,
      MAX(Geschosszahl) AS Geschosszahl,
      MAX(Geschosshoehe) AS Geschosshoehe,
      MAX(Nutzungsziffer) AS Nutzungsziffer,
      MAX(Ausnuetzung_Art) AS Ausnuetzung_Art,
      MAX(Ausnuetzung_Art_Label) AS Ausnuetzung_Art_Label,
      --MAX(Zonenparameter) AS Zonenparameter,
      MAX(Gruenflaechenziffer) AS Gruenflaechenziffer,
      MAX(Gesamthoehe) AS Gesamthoehe,
      MAX(Anteil_Arbeitsnutzung) AS Anteil_Arbeitsnutzung,
      MAX(Anteil_Wohnnutzung) AS Anteil_Wohnnutzung,              
      MAX(Platzbedarf_Arbeitsnutzung) AS Platzbedarf_Arbeitsnutzung,
      MAX(Platzbedarf_Wohnnutzung) AS Platzbedarf_Wohnnutzung
  
    FROM 
    (
      SELECT 
          sid
          ,zone_code
          ,CASE WHEN parameter_name = 'Grosser_Grenzabstand' THEN CAST(parameter_wert AS real) ELSE NULL END AS Grosser_Grenzabstand
          ,CASE WHEN parameter_name = 'Kleiner_Grenzabstand' THEN CAST(parameter_wert AS real) ELSE NULL END AS Kleiner_Grenzabstand
          ,CASE WHEN parameter_name = 'Geschosszahl' THEN CAST(parameter_wert AS real) ELSE NULL END AS Geschosszahl
          ,CASE WHEN parameter_name = 'Geschosshoehe' THEN CAST(parameter_wert AS real) ELSE NULL END AS Geschosshoehe
          ,CASE WHEN parameter_name = 'Nutzungsziffer' THEN CAST(parameter_wert AS real) ELSE NULL END AS Nutzungsziffer
          ,CASE WHEN parameter_name = 'Ausnuetzung_Art' THEN CAST(parameter_wert AS real) ELSE NULL END AS Ausnuetzung_Art
          ,CASE WHEN parameter_name = 'Ausnuetzung_Art' THEN (
              CASE WHEN parameter_wert = '0' THEN 'Keine'
                WHEN parameter_wert = '1' THEN 'GFZ'
                WHEN parameter_wert = '2' THEN 'BMZ'
                WHEN parameter_wert = '3' THEN 'ÜZ'
                ELSE ''
              END
              ) ELSE NULL END AS Ausnuetzung_Art_Label
          --,CASE WHEN parameter_name = 'Zonenparameter' THEN parameter_wert ELSE NULL END AS Zonenparameter
                            
          ,CASE WHEN parameter_name = 'Gruenflaechenziffer' THEN CAST(parameter_wert AS real) ELSE NULL END AS Gruenflaechenziffer
          ,CASE WHEN parameter_name = 'Gesamthoehe' THEN CAST(parameter_wert AS real) ELSE NULL END AS Gesamthoehe
          ,CASE WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Anteil_Arbeitsnutzung
          ,CASE WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN 1-CAST(parameter_wert AS real) ELSE NULL END AS Anteil_Wohnnutzung
          ,CASE WHEN parameter_name = 'Platzbedarf_Arbeitsnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Platzbedarf_Arbeitsnutzung
          ,CASE WHEN parameter_name = 'Platzbedarf_Wohnnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Platzbedarf_Wohnnutzung
        FROM dencity.szenario_parameter
        WHERE szenario_parameter.sid BETWEEN 20 AND 39
     ) AS p
     GROUP BY sid, zone_code
  ) AS pv ON pv.sid = kg.sid AND pv.zone_code = ki.zone_code

  LEFT JOIN dencity.szenario ON kg.sid = szenario.id
  
  ORDER BY 1





---
--- By Zone v2
---
--Global
SELECT 
    CASE WHEN szenario.pid = 4 THEN 'Langenthal' ELSE 'Uettligen' END AS Projekt
    ,szenario.titel
    ,ki.zone_code
    ,kg.anrechenbare_grundstuecksflaeche
    ,kg.area_gebaeude
    ,kg.volumen
    ,kg.geschossflaeche
    ,kg.gfz
    ,ki.avgGFZ
    ,kg.bmz 
    ,ki.avgBMZ
    ,kg.uz
    ,ki.avgUZ
    ,pv.Ausnuetzung_Art_Label    
    ,pv.Nutzungsziffer    

    ,ki.minUZ
    ,ki.minGFZ
    ,ki.minBMZ
    ,ki.maxUZ
    ,ki.maxGFZ
    ,ki.maxBMZ
    ,pv.Grosser_Grenzabstand
    ,pv.Kleiner_Grenzabstand
    ,pv.Geschosszahl    
    ,pv.Geschosshoehe
    ,pv.Ausnuetzung_Art    
    ,pv.Gruenflaechenziffer    
    ,pv.Gesamthoehe    
    ,pv.Anteil_Arbeitsnutzung
    ,pv.Anteil_Wohnnutzung    
    ,pv.Platzbedarf_Arbeitsnutzung    
    ,pv.Platzbedarf_Wohnnutzung
    ,kg.area_parzelle

    ,szenario.pid
    ,kg.sid

FROM
(SELECT 
    szenario.id AS sid
	,zone_code
    ,anrechenbare_grundstuecksflaeche
    ,area_parzelle
    ,area_gebaeude
    ,geschossflaeche
    ,volumen
    ,COALESCE(volumen,0)/anrechenbare_grundstuecksflaeche AS bmz
    ,COALESCE(geschossflaeche,0)/anrechenbare_grundstuecksflaeche AS gfz
    ,COALESCE(area_gebaeude,0)/anrechenbare_grundstuecksflaeche AS uz
  FROM dencity.szenario
  LEFT JOIN 
    (
	SELECT 
          zone_code
		 ,sid
         ,SUM(anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche
         ,SUM(area_parzelle) AS area_parzelle 
		 ,SUM(area_gebaeude) AS area_gebaeude
		 ,SUM(geschossflaeche)  AS geschossflaeche
		 ,SUM(volumen)  AS volumen
		 FROM 
	(SELECT
         parzelle.gid
		 ,zone_code
		 ,szenario.id AS sid
         ,MIN(parzelle.anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche
         ,MIN(ST_Area(parzelle.geom)) AS area_parzelle 
		 ,SUM(ST_Area(ST_Intersection(parzelle.geom,gebaeude.geom))) AS area_gebaeude
		 ,SUM(ST_Area(ST_Intersection(parzelle.geom,gebaeude.geom))*geschosszahl)  AS geschossflaeche
		 ,SUM(ST_Area(ST_Intersection(parzelle.geom,gebaeude.geom))*hoehe)  AS volumen
       FROM dencity.parzelle 
       LEFT JOIN dencity.szenario ON parzelle.pid = szenario.pid
	   LEFT JOIN dencity.gebaeude ON gebaeude.sid = szenario.id AND ST_Intersects(parzelle.geom,gebaeude.geom)
	   WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39 
	   GROUP BY parzelle.gid, zone_code, szenario.id) AS pg  GROUP BY sid, zone_code
	   
	   )
	  
	  AS pc ON szenario.id = pc.sid
	   WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39
 )
  AS kg
LEFT JOIN   
--Individual
(SELECT 
sid,
zone_code,
AVG(uz) AS avgUZ,
AVG(gfz) AS avgGFZ,
AVG(bmz) AS avgBMZ,
MIN(uz) AS minUZ,
MIN(gfz) AS minGFZ,
MIN(bmz) AS minBMZ,
MAX(uz) AS maxUZ,
MAX(gfz) AS maxGFZ,
MAX(bmz) AS maxBMZ
FROM
(SELECT sid, pgid, zone_code,
    SUM(gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS uz,
    SUM(geschosszahl*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS gfz,
    SUM(hoehe*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS bmz
FROM
(
SELECT 
    szenario.id AS sid,
    parzelle.gid AS pgid,
    parzelle.nummer,
    parzelle.anrechenbare_grundstuecksflaeche,
	parzelle.zone_code,
    ST_Area(parzelle.geom) AS parzelle_area,
    COALESCE(gebaeude.hoehe,0) AS hoehe,
    COALESCE(gebaeude.geschosszahl,0) AS geschosszahl,
    gebaeude.geschossflaeche,
    gebaeude.nutzer_wohnen,
    gebaeude.nutzer_arbeit,
    COALESCE(ST_Area(ST_Intersection(gebaeude.geom,parzelle.geom)),0) gebaeude_area,
    gebaeude.anteil_arbeitsnutzung
  FROM dencity.szenario
  LEFT JOIN dencity.parzelle ON szenario.pid = parzelle.pid
  LEFT JOIN dencity.gebaeude ON gebaeude.sid = szenario.id AND ST_Intersects(parzelle.geom, gebaeude.geom)
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 20 AND 39
) AS i1
GROUP BY sid, pgid, zone_code
) AS i2
GROUP BY sid, zone_code) AS ki ON kg.sid = ki.sid AND ki.zone_code = kg.zone_code
 
LEFT JOIN
--Parameter
  (SELECT sid, 
      zone_code,
      MAX(Grosser_Grenzabstand) AS Grosser_Grenzabstand,
      MAX(Kleiner_Grenzabstand) AS Kleiner_Grenzabstand,
      MAX(Geschosszahl) AS Geschosszahl,
      MAX(Geschosshoehe) AS Geschosshoehe,
      MAX(Nutzungsziffer) AS Nutzungsziffer,
      MAX(Ausnuetzung_Art) AS Ausnuetzung_Art,
      MAX(Ausnuetzung_Art_Label) AS Ausnuetzung_Art_Label,
      --MAX(Zonenparameter) AS Zonenparameter,
      MAX(Gruenflaechenziffer) AS Gruenflaechenziffer,
      MAX(Gesamthoehe) AS Gesamthoehe,
      MAX(Anteil_Arbeitsnutzung) AS Anteil_Arbeitsnutzung,
      MAX(Anteil_Wohnnutzung) AS Anteil_Wohnnutzung,              
      MAX(Platzbedarf_Arbeitsnutzung) AS Platzbedarf_Arbeitsnutzung,
      MAX(Platzbedarf_Wohnnutzung) AS Platzbedarf_Wohnnutzung
  
    FROM 
    (
      SELECT 
          sid
          ,zone_code
          ,CASE WHEN parameter_name = 'Grosser_Grenzabstand' THEN CAST(parameter_wert AS real) ELSE NULL END AS Grosser_Grenzabstand
          ,CASE WHEN parameter_name = 'Kleiner_Grenzabstand' THEN CAST(parameter_wert AS real) ELSE NULL END AS Kleiner_Grenzabstand
          ,CASE WHEN parameter_name = 'Geschosszahl' THEN CAST(parameter_wert AS real) ELSE NULL END AS Geschosszahl
          ,CASE WHEN parameter_name = 'Geschosshoehe' THEN CAST(parameter_wert AS real) ELSE NULL END AS Geschosshoehe
          ,CASE WHEN parameter_name = 'Nutzungsziffer' THEN CAST(parameter_wert AS real) ELSE NULL END AS Nutzungsziffer
          ,CASE WHEN parameter_name = 'Ausnuetzung_Art' THEN CAST(parameter_wert AS real) ELSE NULL END AS Ausnuetzung_Art
          ,CASE WHEN parameter_name = 'Ausnuetzung_Art' THEN (
              CASE WHEN parameter_wert = '0' THEN 'Keine'
                WHEN parameter_wert = '1' THEN 'GFZ'
                WHEN parameter_wert = '2' THEN 'BMZ'
                WHEN parameter_wert = '3' THEN 'ÜZ'
                ELSE ''
              END
              ) ELSE NULL END AS Ausnuetzung_Art_Label
          --,CASE WHEN parameter_name = 'Zonenparameter' THEN parameter_wert ELSE NULL END AS Zonenparameter
                            
          ,CASE WHEN parameter_name = 'Gruenflaechenziffer' THEN CAST(parameter_wert AS real) ELSE NULL END AS Gruenflaechenziffer
          ,CASE WHEN parameter_name = 'Gesamthoehe' THEN CAST(parameter_wert AS real) ELSE NULL END AS Gesamthoehe
          ,CASE WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Anteil_Arbeitsnutzung
          ,CASE WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN 1-CAST(parameter_wert AS real) ELSE NULL END AS Anteil_Wohnnutzung
          ,CASE WHEN parameter_name = 'Platzbedarf_Arbeitsnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Platzbedarf_Arbeitsnutzung
          ,CASE WHEN parameter_name = 'Platzbedarf_Wohnnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Platzbedarf_Wohnnutzung
        FROM dencity.szenario_parameter
        WHERE szenario_parameter.sid BETWEEN 20 AND 39
     ) AS p
     GROUP BY sid, zone_code
  ) AS pv ON pv.sid = kg.sid AND pv.zone_code = ki.zone_code

  LEFT JOIN dencity.szenario ON kg.sid = szenario.id
  
  ORDER BY sid, zone_code







