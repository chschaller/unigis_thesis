DO $$
DECLARE  
  i integer := 0;
  configurationCounter integer := 1;  
  ----Upper bound
  --ausnutzungArten integer[] := ARRAY[0,1,2,3];
  --ausnutzungen real[] := ARRAY[0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0];
  --gruenflaechenziffern real[] := ARRAY[0.0,0.25,0.5,0.75,1.0];
  --kleineGrenzabstaende integer[] := ARRAY[2,4,6];
  --grosseGrenzabstaende integer[] := ARRAY[2,4,6,8,10];
  --geschosszahlen integer[] := ARRAY[1,2,3,4,5];
  --geschosshoehen real[] := ARRAY[2.8,3.0,3.4];
  --zonen varchar[] := ARRAY['Z1','Z2','Z3','Z4','Z5'];

  --Sensible/Animation
  ausnutzungArten integer[] := ARRAY[0,1,2,3];
  ausnutzungen real[] := ARRAY[0.5,0.75,1,1.25,1.5];
  gruenflaechenziffern real[] := ARRAY[0.0,0.5];
  kleineGrenzabstaende integer[] := ARRAY[2,4,6];
  grosseGrenzabstaende integer[] := ARRAY[6,8,10];
  geschosszahlen integer[] := ARRAY[2,3,4];
  geschosshoehen real[] := ARRAY[2.8];
  zonenIndexe integer[] := ARRAY[1,2,3,4,5];
  zonen varchar[] := ARRAY['Z1','Z2','Z3','Z4','Z5'];

  parameterConfiguration varchar[] := ARRAY[''];
  parameterUngruppiert varchar[] := ARRAY['{}','{}','{}','{}','{}'];
  parameterGruppiert varchar[] := ARRAY['{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["3965","3951"]},
{"typ": "reihe","parzellen_nummern": ["3966","3967"]},
{"typ": "reihe","parzellen_nummern": ["3968","3969"]},
{"typ": "reihe","parzellen_nummern": ["4278","4279"]}
  ]
}','{
  "gruppen": [
{"typ": "reihe","parzellen_nummern": ["5020","5888"]},
{"typ": "reihe","parzellen_nummern": ["5016","5131"]},
{"typ": "reihe","parzellen_nummern": ["5862","5863","5018"]},
{"typ": "reihe","parzellen_nummern": ["5426","3990"]},
{"typ": "reihe","parzellen_nummern": ["5871","5872"]},
{"typ": "reihe","parzellen_nummern": ["5797","5797"]},
{"typ": "reihe","parzellen_nummern": ["5794","5795"]},
{"typ": "reihe","parzellen_nummern": ["3874","5733"]},
{"typ": "reihe","parzellen_nummern": ["4936","4679"]},
{"typ": "reihe","parzellen_nummern": ["4454","4547","4546"]},
{"typ": "reihe","parzellen_nummern": ["4543","4542","4541","4455"]},
{"typ": "reihe","parzellen_nummern": ["4545","4544"]},
{"typ": "reihe","parzellen_nummern": ["4493","4492","4491","4490"]},
{"typ": "reihe","parzellen_nummern": ["4645","4644","4643","3863"]},
{"typ": "reihe","parzellen_nummern": ["2550","4646"]},
{"typ": "reihe","parzellen_nummern": ["4495","4494"]},
{"typ": "reihe","parzellen_nummern": ["4642","5143"]},
{"typ": "reihe","parzellen_nummern": ["4934","4935"]},
{"typ": "reihe","parzellen_nummern": ["4905","4906"]},
{"typ": "reihe","parzellen_nummern": ["4808","4992"]},
{"typ": "reihe","parzellen_nummern": ["4990","5163"]},
{"typ": "reihe","parzellen_nummern": ["5164","5165","5166"]},
{"typ": "reihe","parzellen_nummern": ["4956","4955","4954","2779"]},
{"typ": "reihe","parzellen_nummern": ["4383","4678","4677"]},
{"typ": "reihe","parzellen_nummern": ["4400","4401","4530","4531"]},
{"typ": "reihe","parzellen_nummern": ["6159","3572"]},
{"typ": "reihe","parzellen_nummern": ["5874","5875","5876","5877"]},
{"typ": "reihe","parzellen_nummern": ["5686","5156"]},
{"typ": "reihe","parzellen_nummern": ["2691","5141"]},
{"typ": "reihe","parzellen_nummern": ["5142","5157"]},
{"typ": "reihe","parzellen_nummern": ["4869","4466"]},
{"typ": "reihe","parzellen_nummern": ["4591","4467"]},
{"typ": "reihe","parzellen_nummern": ["2658","5127"]},
{"typ": "reihe","parzellen_nummern": ["5128","5040"]},
{"typ": "reihe","parzellen_nummern": ["2623","3308","2624"]},
{"typ": "reihe","parzellen_nummern": ["3587","3584"]},
{"typ": "reihe","parzellen_nummern": ["3586","3583"]},
{"typ": "reihe","parzellen_nummern": ["3585","3582"]},
{"typ": "reihe","parzellen_nummern": ["4409","4484"]},
{"typ": "reihe","parzellen_nummern": ["4485","4486"]},
{"typ": "reihe","parzellen_nummern": ["4487","4488"]},
{"typ": "reihe","parzellen_nummern": ["5043","5766","5767"]},
{"typ": "reihe","parzellen_nummern": ["4410","4750"]},
{"typ": "reihe","parzellen_nummern": ["2617","5893","5894","5895"]},
{"typ": "reihe","parzellen_nummern": ["5821","5822"]},
{"typ": "reihe","parzellen_nummern": ["5774","5817","5818","5819","5820"]},
{"typ": "reihe","parzellen_nummern": ["5925","5926"]}
  ]
}','{}','{}','{}'];

  ausnutzungArt integer := 0;
  ausnutzung real := 0.2;
  gruenflaechenziffer real := 0.0;
  kleinerGrenzabstand integer := 2;
  grosserGrenzabstand integer := 2;
  gebaeudehoehe real := 10;
  geschosszahl integer := 1;
  geschosshoehe real := 3.0;

  --1;"Dotzigen", 2;"Bern: Belvedère Länggasse", 4;"Langenthal", 5;"Steffisburg", 3;"Wohlen: Uettligen"
  projekt integer := 3;
  szenario integer := 2;
  szenarioTitel varchar(50) := '';
  szenarioBeschreibung varchar(512) := '';
  ausnutzungArtLabel varchar(30) := '';
  configurationLabel varchar(30) := '';
  zone varchar := '';
  zoneParameter varchar := '';
  zoneIndex integer := 1;
  v_nk RECORD;
BEGIN  

  --DELETE FROM dencity.szenario_parameter WHERE sid IN (SELECT id FROM dencity.szenario WHERE pid = projekt AND titel LIKE 'g_%');

  --DELETE FROM dencity.szenario WHERE pid = projekt AND titel LIKE 'g_%';

  FOREACH ausnutzungArt IN ARRAY ausnutzungArten
  LOOP
    FOREACH kleinerGrenzabstand IN ARRAY kleineGrenzabstaende
    LOOP
      FOREACH grosserGrenzabstand IN ARRAY grosseGrenzabstaende
      LOOP
        IF grosserGrenzabstand>=kleinerGrenzabstand THEN
          FOREACH geschosszahl IN ARRAY geschosszahlen
          LOOP
            gebaeudehoehe := 3*(geschosszahl+1.5);
            FOREACH geschosshoehe IN ARRAY geschosshoehen
            LOOP
              FOREACH ausnutzung IN ARRAY ausnutzungen
              LOOP
                --IF ausnutzungArt<>3 OR (ausnutzungArt=3 AND ausnutzung<=1.0) THEN
                  FOREACH gruenflaechenziffer IN ARRAY gruenflaechenziffern
                  LOOP

                    configurationCounter := 1;
                    WHILE configurationCounter < 3 LOOP
                      IF configurationCounter = 1 THEN parameterConfiguration := parameterUngruppiert; configurationLabel := 'Keine'; END IF;
                      IF configurationCounter = 2 THEN parameterConfiguration := parameterGruppiert; configurationLabel := 'Gruppiert';  END IF;

                      i := i + 1;
                      --Keine = 0, GFZ   = 1, BMZ   = 2, ÜZ    = 3
                      ausnutzungArtLabel := CASE WHEN ausnutzungArt = 1 THEN 'GFZ' WHEN ausnutzungArt = 2 THEN 'BMZ' WHEN ausnutzungArt = 3 THEN 'ÜZ' ELSE 'Keine' END;  
                      szenarioTitel := 'g_' || CAST(i AS varchar) || '_' || ausnutzungArtLabel || '_' || ausnutzung || '_' || grosserGrenzabstand || '_' || kleinerGrenzabstand || '_' || geschosszahl || '_' ||  geschosshoehe || '_' || gebaeudehoehe || '_' || gruenflaechenziffer;
                      szenarioBeschreibung := 'Nummer:' || CAST(i AS varchar) || '; Ausnützung Art:' || ausnutzungArtLabel || '; Ausnutzung: ' || ausnutzung || '; Grosser Grenzabstand:' || grosserGrenzabstand || '; Kleiner Grenzabstand:' || kleinerGrenzabstand || '; Geschosszahl:' || geschosszahl || '; Geschosshöhe:' || geschosshoehe || '; Maximale Gebäudehöhe:' || gebaeudehoehe || '; Grünflächenziffer:' || gruenflaechenziffer|| '; Gruppierung:' || configurationLabel;
                                    
                      INSERT INTO dencity.szenario(titel, beschreibung, pid) VALUES (szenarioTitel, szenarioTitel, projekt) RETURNING id INTO szenario;

                      FOREACH zoneIndex IN ARRAY zonenIndexe
                      LOOP
                        zone := zonen[zoneIndex];
                        zoneParameter := parameterConfiguration[zoneIndex];
  
                        --"Ausnuetzung_Art"
                        --"Kleiner_Grenzabstand"
                        --"Grosser_Grenzabstand"
                        --"Zonenparameter"
                        --"Gruenflaechenziffer"
                        --"Gesamthoehe"
                        --"Anteil_Arbeitsnutzung"
                        --"Platzbedarf_Arbeitsnutzung"
                        --"Platzbedarf_Wohnnutzung"
                        --"Nutzungsziffer"
                        --"Geschosshoehe"
                        --"Geschosszahl"
                        
                        --Parametrized
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Ausnuetzung_Art', CAST(ausnutzungArt AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Kleiner_Grenzabstand', CAST(kleinerGrenzabstand AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Grosser_Grenzabstand', CAST(grosserGrenzabstand AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Geschosshoehe', CAST(geschosshoehe AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Geschosszahl', CAST(geschosszahl AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Nutzungsziffer', CAST(ausnutzung AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Gruenflaechenziffer', CAST(gruenflaechenziffer AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Gesamthoehe', CAST(gebaeudehoehe AS VARCHAR(1024)));
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Zonenparameter', zoneParameter);
                        
                        --Fixed
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Anteil_Arbeitsnutzung', '0');
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Platzbedarf_Arbeitsnutzung', '80');
                        INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Platzbedarf_Wohnnutzung', '60');
  
                      END LOOP;
                    
                      configurationCounter := configurationCounter + 1;
                    END LOOP;                      
                  END LOOP; 
                --END IF;                  
              END LOOP;
            END LOOP;
          END LOOP;
        END IF;
      END LOOP;
    END LOOP;
  END LOOP;
  
  
  RAISE NOTICE 'Anzahl Szenarien %', i;
  RAISE NOTICE 'Szenario %', szenarioTitel;
  
END $$;
    