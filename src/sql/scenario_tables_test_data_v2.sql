--
--Insert test data
--
DELETE FROM dencity.szenario;
DELETE FROM dencity.projekt;

INSERT INTO dencity.projekt(id, titel)
    VALUES (1, 'Dotzigen');
INSERT INTO dencity.projekt(id, titel)
    VALUES (2, 'Bern: Belvedère Länggasse');
INSERT INTO dencity.projekt(id, titel)
    VALUES (3, 'Wohlen: Uettligen');
INSERT INTO dencity.projekt(id, titel)
    VALUES (4, 'Langenthal');
INSERT INTO dencity.projekt(id, titel)
    VALUES (5, 'Steffisburg');    

INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis', 1);
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis W3', 1);    
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis', 2);
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis W3', 2);    
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis W4', 2);    
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis', 3);
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis', 4);
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Szenario 2', 4);
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Basis', 5);
INSERT INTO dencity.szenario(titel, pid)
    VALUES ('Szenario 2', 5);
    
INSERT INTO dencity.szenario_parameter VALUES (15, 1, 'Z3', 'Grosser_Grenzabstand', '4.964', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (16, 1, 'Z3', 'Kleiner_Grenzabstand', '1.5', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (17, 1, 'Z3', 'Geschosszahl', '3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (18, 1, 'Z3', 'Geschosshoehe', '3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (19, 1, 'Z3', 'Nutzungsziffer', '0.3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (20, 1, 'Z3', 'Zonenparameter', '{}', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (21, 1, 'Z3', 'Asnuetzung_Art', '1', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (22, 1, 'Z4', 'Grosser_Grenzabstand', '3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (23, 1, 'Z4', 'Kleiner_Grenzabstand', '2', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (24, 1, 'Z4', 'Geschosszahl', '3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (25, 1, 'Z4', 'Geschosshoehe', '4', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (26, 1, 'Z4', 'Nutzungsziffer', '0.7', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (27, 1, 'Z4', 'Zonenparameter', '{ "gruppen": [{"typ": "block", "parzellen_nummern": ["106"]}]}', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (28, 1, 'Z4', 'Asnuetzung_Art', '1', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (29, 1, 'Z5', 'Grosser_Grenzabstand', '10', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (30, 1, 'Z5', 'Kleiner_Grenzabstand', '10', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (31, 1, 'Z5', 'Geschosszahl', '1', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (32, 1, 'Z5', 'Geschosshoehe', '2.5', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (33, 1, 'Z5', 'Nutzungsziffer', '0.6', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (34, 1, 'Z5', 'Zonenparameter', '{}', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (35, 1, 'Z5', 'Asnuetzung_Art', '1', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (141, 2, 'Z1', 'Grosser_Grenzabstand', '4', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (142, 2, 'Z1', 'Kleiner_Grenzabstand', '2.091', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (143, 2, 'Z1', 'Geschosszahl', '2', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (144, 2, 'Z1', 'Geschosshoehe', '3.5', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (145, 2, 'Z1', 'Nutzungsziffer', '0.5', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (146, 2, 'Z1', 'Zonenparameter', '{ "gruppen": [{"typ": "reihe", "parzellen_nummern": ["221", "754", "755"]},
{"typ": "reihe", "parzellen_nummern": ["650","649","648","647"]},
{"typ": "reihe", "parzellen_nummern": ["645","640","639","638"]},
{"typ": "block", "parzellen_nummern": ["217","28","237","258","382","357","356","134","307"]},
{"typ": "block", "parzellen_nummern": ["142","766","765","763","128","121","393","130","569"]}
]}', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (147, 2, 'Z1', 'Asnuetzung_Art', '1', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (148, 2, 'Z2', 'Grosser_Grenzabstand', '4', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (149, 2, 'Z2', 'Kleiner_Grenzabstand', '2.5', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (150, 2, 'Z2', 'Geschosszahl', '3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (151, 2, 'Z2', 'Geschosshoehe', '3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (152, 2, 'Z2', 'Nutzungsziffer', '1.5', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (153, 2, 'Z2', 'Zonenparameter', '{ "gruppen": [{"typ": "block", "parzellen_nummern": ["49"]}]}
', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (154, 2, 'Z2', 'Asnuetzung_Art', '1', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (1, 1, 'Z1', 'Grosser_Grenzabstand', '4', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (2, 1, 'Z1', 'Kleiner_Grenzabstand', '2.091', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (3, 1, 'Z1', 'Geschosszahl', '2', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (4, 1, 'Z1', 'Geschosshoehe', '3.5', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (5, 1, 'Z1', 'Nutzungsziffer', '0.5', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (6, 1, 'Z1', 'Zonenparameter', '{ "gruppen": [{"typ": "reihe", "parzellen_nummern": ["221", "754", "755"]},
{"typ": "reihe", "parzellen_nummern": ["650","649","648","647"]},
{"typ": "reihe", "parzellen_nummern": ["645","640","639","638"]},
{"typ": "block", "parzellen_nummern": ["217","28","237","258","382","357","356","134","307"]},
{"typ": "block", "parzellen_nummern": ["142","766","765","763","128","121","393","130","569"]}
]}', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (7, 1, 'Z1', 'Asnuetzung_Art', '1', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (8, 1, 'Z2', 'Grosser_Grenzabstand', '4', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (9, 1, 'Z2', 'Kleiner_Grenzabstand', '2.5', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (10, 1, 'Z2', 'Geschosszahl', '3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (11, 1, 'Z2', 'Geschosshoehe', '3', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (12, 1, 'Z2', 'Nutzungsziffer', '1.5', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (13, 1, 'Z2', 'Zonenparameter', '{ "gruppen": [{"typ": "block", "parzellen_nummern": ["49"]}]}
', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (14, 1, 'Z2', 'Asnuetzung_Art', '1', '2018-01-04 13:06:28.113995');
INSERT INTO dencity.szenario_parameter VALUES (155, 2, 'Z3', 'Grosser_Grenzabstand', '4.964', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (156, 2, 'Z3', 'Kleiner_Grenzabstand', '1.5', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (157, 2, 'Z3', 'Geschosszahl', '3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (158, 2, 'Z3', 'Geschosshoehe', '3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (159, 2, 'Z3', 'Nutzungsziffer', '0.3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (160, 2, 'Z3', 'Zonenparameter', '{}', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (161, 2, 'Z3', 'Asnuetzung_Art', '1', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (162, 2, 'Z4', 'Grosser_Grenzabstand', '3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (163, 2, 'Z4', 'Kleiner_Grenzabstand', '2', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (164, 2, 'Z4', 'Geschosszahl', '3', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (165, 2, 'Z4', 'Geschosshoehe', '4', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (166, 2, 'Z4', 'Nutzungsziffer', '0.7', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (167, 2, 'Z4', 'Zonenparameter', '{ "gruppen": [{"typ": "block", "parzellen_nummern": ["106"]}]}', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (168, 2, 'Z4', 'Asnuetzung_Art', '1', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (169, 2, 'Z5', 'Grosser_Grenzabstand', '10', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (170, 2, 'Z5', 'Kleiner_Grenzabstand', '10', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (171, 2, 'Z5', 'Geschosszahl', '1', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (172, 2, 'Z5', 'Geschosshoehe', '2.5', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (173, 2, 'Z5', 'Nutzungsziffer', '0.6', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (174, 2, 'Z5', 'Zonenparameter', '{}', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (175, 2, 'Z5', 'Asnuetzung_Art', '1', '2018-01-04 13:08:38.348115');
INSERT INTO dencity.szenario_parameter VALUES (176, 7, 'Z1', 'Grosser_Grenzabstand', '5', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (177, 7, 'Z1', 'Kleiner_Grenzabstand', '3', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (178, 7, 'Z1', 'Geschosszahl', '3', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (179, 7, 'Z1', 'Geschosshoehe', '2.9', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (180, 7, 'Z1', 'Nutzungsziffer', '1.5', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (181, 7, 'Z1', 'Zonenparameter', '{}', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (182, 7, 'Z1', 'Asnuetzung_Art', '0', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (183, 7, 'Z2', 'Grosser_Grenzabstand', '6', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (184, 7, 'Z2', 'Kleiner_Grenzabstand', '3', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (185, 7, 'Z2', 'Geschosszahl', '2', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (186, 7, 'Z2', 'Geschosshoehe', '2.9', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (187, 7, 'Z2', 'Nutzungsziffer', '0.5', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (188, 7, 'Z2', 'Zonenparameter', '{}', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (189, 7, 'Z2', 'Asnuetzung_Art', '1', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (190, 7, 'Z3', 'Grosser_Grenzabstand', '4', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (191, 7, 'Z3', 'Kleiner_Grenzabstand', '2.5', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (192, 7, 'Z3', 'Geschosszahl', '3', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (193, 7, 'Z3', 'Geschosshoehe', '2.9', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (194, 7, 'Z3', 'Nutzungsziffer', '2', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (195, 7, 'Z3', 'Zonenparameter', '{}', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (196, 7, 'Z3', 'Asnuetzung_Art', '0', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (197, 7, 'Z4', 'Grosser_Grenzabstand', '5', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (198, 7, 'Z4', 'Kleiner_Grenzabstand', '2', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (199, 7, 'Z4', 'Geschosszahl', '2', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (200, 7, 'Z4', 'Geschosshoehe', '2.9', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (201, 7, 'Z4', 'Nutzungsziffer', '0.75', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (202, 7, 'Z4', 'Zonenparameter', '{}', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (203, 7, 'Z4', 'Asnuetzung_Art', '1', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (204, 7, 'Z5', 'Grosser_Grenzabstand', '4', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (205, 7, 'Z5', 'Kleiner_Grenzabstand', '2', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (206, 7, 'Z5', 'Geschosszahl', '4', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (207, 7, 'Z5', 'Geschosshoehe', '2.9', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (208, 7, 'Z5', 'Nutzungsziffer', '0.8', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (209, 7, 'Z5', 'Zonenparameter', '{}', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (210, 7, 'Z5', 'Asnuetzung_Art', '1', '2018-01-13 09:13:58.546389');
INSERT INTO dencity.szenario_parameter VALUES (246, 8, 'Z1', 'Grosser_Grenzabstand', '5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (247, 8, 'Z1', 'Kleiner_Grenzabstand', '3', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (248, 8, 'Z1', 'Geschosszahl', '3', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (249, 8, 'Z1', 'Geschosshoehe', '2.9', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (250, 8, 'Z1', 'Nutzungsziffer', '1.5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (251, 8, 'Z1', 'Zonenparameter', '{}', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (252, 8, 'Z1', 'Asnuetzung_Art', '0', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (253, 8, 'Z2', 'Grosser_Grenzabstand', '6', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (254, 8, 'Z2', 'Kleiner_Grenzabstand', '3', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (255, 8, 'Z2', 'Geschosszahl', '2', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (256, 8, 'Z2', 'Geschosshoehe', '2.9', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (257, 8, 'Z2', 'Nutzungsziffer', '0.5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (258, 8, 'Z2', 'Zonenparameter', '{ "gruppen": [{"typ": "reihe", "parzellen_nummern": ["600", "4332", "4333", "4334"]},
{"typ": "reihe", "parzellen_nummern": ["528", "1550", "1319", "1221"]},
{"typ": "reihe", "parzellen_nummern": ["527", "1338", "1339", "1193"]}
]}', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (259, 8, 'Z2', 'Asnuetzung_Art', '1', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (260, 8, 'Z3', 'Grosser_Grenzabstand', '4', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (261, 8, 'Z3', 'Kleiner_Grenzabstand', '2.5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (262, 8, 'Z3', 'Geschosszahl', '3', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (263, 8, 'Z3', 'Geschosshoehe', '2.9', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (264, 8, 'Z3', 'Nutzungsziffer', '2', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (265, 8, 'Z3', 'Zonenparameter', '{}', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (266, 8, 'Z3', 'Asnuetzung_Art', '0', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (267, 8, 'Z4', 'Grosser_Grenzabstand', '5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (268, 8, 'Z4', 'Kleiner_Grenzabstand', '2', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (269, 8, 'Z4', 'Geschosszahl', '2', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (270, 8, 'Z4', 'Geschosshoehe', '2.9', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (271, 8, 'Z4', 'Nutzungsziffer', '0.5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (272, 8, 'Z4', 'Zonenparameter', '{}', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (273, 8, 'Z4', 'Asnuetzung_Art', '1', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (274, 8, 'Z5', 'Grosser_Grenzabstand', '4', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (275, 8, 'Z5', 'Kleiner_Grenzabstand', '2', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (276, 8, 'Z5', 'Geschosszahl', '6', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (277, 8, 'Z5', 'Geschosshoehe', '3.5', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (278, 8, 'Z5', 'Nutzungsziffer', '0.7', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (279, 8, 'Z5', 'Zonenparameter', '{}', '2018-01-15 07:10:01.517374');
INSERT INTO dencity.szenario_parameter VALUES (280, 8, 'Z5', 'Asnuetzung_Art', '1', '2018-01-15 07:10:01.517374');

SELECT pg_catalog.setval('dencity.szenario_parameter_id_seq', 350, true);

INSERT INTO dencity.abstandslinie VALUES (3, 1, false, '01020000200808000005000000D227CB1EC4C74341D62D51A90A993241EBBF69ECB9C7434157D2698008993241DA547346B3C743417B76065101993241DEFAE2EEB1C743415E3C79CEFD9832417734B447B1C74341099624EEF7983241');
INSERT INTO dencity.abstandslinie VALUES (2, 1, true, '01020000200808000004000000C8E81B16A2C7434136BB4021B8983241E574F1C3ACC743412CD98B3EA9983241D8CEF723B6C74341305EBA499D9832411117988AC1C74341E5093E778E983241');
INSERT INTO dencity.abstandslinie VALUES (1, 1, false, '0102000020080800000B00000032EB31D9CAC74341855DC3697F983241935603DECCC743415D63B8298798324150E3A57BD2C7434110D9CE379B98324151BACF1BD9C74341323353F2B3983241CE7B1417DBC743413193090CBB98324115603176DBC74341180F9E3FC0983241FD137D56DBC74341A15A61F4C498324192F19AE4D9C7434105685F96C89832419A98ED4FD2C74341ADF75954DA98324164768DF4CAC74341306038F6EE983241C1734A09C1C74341A6B9E37E0C993241');

SELECT pg_catalog.setval('dencity.abstandslinie_gid_seq', 10, true);


--Insert Parcels for Dotzigen
DELETE FROM dencity.parzelle WHERE pid=1;

INSERT INTO dencity.parzelle(pid, nummer, zone_code, anrechenbare_grundstuecksflaeche, geom)
SELECT pid, nummer, zone_code, SUM(anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche, MIN(geom) AS geom
FROM
(
SELECT 1 AS pid, nummer AS nummer,  
       (SELECT j.zone_lo FROM (SELECT i.zone_lo, ST_Area(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(i.geom))) AS area FROM be.uzp_bauzonen AS i WHERE ST_Intersects(mopube_grundstueck.geom,ST_Force2D(i.geom))) AS j ORDER BY j.area DESC LIMIT 1 )  AS zone_code,
       ST_Area(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(uzp_bauzonen.geom))) AS anrechenbare_grundstuecksflaeche,
       mopube_grundstueck.geom
  FROM be.mopube_grundstueck, be.uzp_bauzonen
  WHERE bfsnr = 386 
  AND ST_Intersects(mopube_grundstueck.geom,uzp_bauzonen.geom) 
  AND bfs = 386 
  AND nummer NOT IN ('15','17','18','25','66','94','152','215','278','282','346','399','404','424','442','492','544','559','560','561','562','563','564','567','575','572','578','580','588','641','646','675','688','689','714','779','832','853') 
) AS t
WHERE anrechenbare_grundstuecksflaeche>1
GROUP BY pid, nummer, zone_code
ORDER BY 2;

--Remap Zones
UPDATE dencity.parzelle
SET
zone_code=
    CASE 
      WHEN zone_code = 'Zone für öffentliche Nutzung' THEN 'Z5'
      WHEN zone_code = 'Bestehende Überbauungsordnung UeO' THEN 'Z2'
      WHEN zone_code = 'Zone für Sport und Freizeit ZSF' THEN 'Z5'
      WHEN zone_code = 'Wohnzone W1' THEN 'Z1'
      WHEN zone_code = 'Gewerbebereich West' THEN 'Z4'
      WHEN zone_code = 'Gewerbebereich Ost' THEN 'Z4'
      WHEN zone_code = 'Wohn- und Gewerbezone 2' THEN 'Z4'
      WHEN zone_code = 'Dorfzone A' THEN 'Z1'
      WHEN zone_code = 'Wohnzone W2' THEN 'Z1'
      WHEN zone_code = 'Grünzone GrZ' THEN 'Z5'
      WHEN zone_code = 'Wohnbereich Ost' THEN 'Z1'
      WHEN zone_code = 'ZPP 7 Schuldrieder' THEN 'Z2'
      WHEN zone_code = 'Zone für Sport und Freizeit' THEN 'Z5'
      ELSE 'Z5'
    END
WHERE pid = 1;
    
--Insert Parcels for Langenthal
DELETE FROM dencity.parzelle WHERE pid=4;
INSERT INTO dencity.parzelle(pid, nummer, zone_code, anrechenbare_grundstuecksflaeche, geom)
SELECT pid, nummer, zone_code, SUM(anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche, MIN(geom) AS geom
FROM
(
SELECT 4 AS pid, nummer AS nummer,  
       (SELECT j.zone_lo FROM (SELECT i.zone_lo, ST_Area(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(i.geom))) AS area FROM be.uzp_bauzonen AS i WHERE ST_Intersects(mopube_grundstueck.geom,ST_Force2D(i.geom))) AS j ORDER BY j.area DESC LIMIT 1 )  AS zone_code,
       ST_Area(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(uzp_bauzonen.geom))) AS anrechenbare_grundstuecksflaeche,
       mopube_grundstueck.geom
  FROM be.mopube_grundstueck, be.uzp_bauzonen
  WHERE bfsnr = 329 
  AND ST_Intersects(mopube_grundstueck.geom,uzp_bauzonen.geom) 
  AND bfs = 329
  AND ST_Within(mopube_grundstueck.geom,ST_SetSRID(ST_GeomFromText('Polygon ((2625757.2294821604155004 1228954.80168768833391368, 2625854.55908969836309552 1228972.35292839189060032, 
2625957.2072550249285996 1229011.178400251083076, 2626069.42882437165826559 1228593.67161381850019097, 
2626131.65595050249248743 1228634.09265301469713449, 2626175.79998015100136399 1228662.28100929595530033, 
2626216.75287512550130486 1228675.04554798942990601, 2626165.69472035160288215 1228584.63006557733751833,
 2626127.40110427141189575 1228399.01239874307066202, 2626023.15737160807475448 1228396.3531198485288769, 
 2625981.67262085387483239 1228382.5248695972841233, 2625864.13249371806159616 1228375.6107444716617465, 
 2625864.66434949729591608 1228376.67445602943189442, 2625757.2294821604155004 1228954.80168768833391368))'),2056)) 
  AND nummer NOT IN ('197.03') 
) AS t
WHERE anrechenbare_grundstuecksflaeche>1
GROUP BY pid, nummer, zone_code
ORDER BY 2;

--Remap Zones
UPDATE dencity.parzelle
SET
zone_code=
    CASE 
      WHEN zone_code = 'Wohnzone W2/ B' THEN 'Z1'
      WHEN zone_code = 'Wohnzone W2/ C' THEN 'Z2'
      WHEN zone_code = 'Mischzone MZ 2' THEN 'Z3'
      WHEN zone_code = 'UeO Nr. 41 "Areal Anliker"' THEN 'Z4'
      WHEN zone_code = 'UeO Nr. 41 "Areal Anliker" Baubereich A'THEN 'Z4'
      WHEN zone_code = 'Zone für öffentliche Nutzung ZöN Art. 77 BauG' THEN 'Z5'
      ELSE 'Z5'
    END
WHERE pid = 4;


UPDATE dencity.parzelle
  SET geom = ST_Multi((SELECT geom FROM (SELECT (ST_Dump(geom)).geom AS geom FROM dencity.parzelle WHERE pid=4 AND nummer ='2166') AS t ORDER BY ST_Area(geom) DESC LIMIT 1))
  WHERE pid=4 AND nummer ='2166';




--Insert Parcels for Uettligen
DELETE FROM dencity.parzelle WHERE pid=3;

INSERT INTO dencity.parzelle(pid, nummer, zone_code, anrechenbare_grundstuecksflaeche, geom)
SELECT pid, nummer, zone_code, SUM(anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche, MIN(geom) AS geom
FROM
(
SELECT 3 AS pid, nummer AS nummer,  
       (SELECT j.zone_lo FROM (SELECT i.zone_lo, ST_Area(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(i.geom))) AS area FROM be.uzp_bauzonen AS i WHERE ST_Intersects(mopube_grundstueck.geom,ST_Force2D(i.geom)) AND i.zone_lo NOT IN ('Landwirtschaftszone / ungezontes Gebiet')) AS j ORDER BY j.area DESC LIMIT 1 )  AS zone_code,
       ST_Area(ST_Intersection(mopube_grundstueck.geom,ST_Force2D(uzp_bauzonen.geom))) AS anrechenbare_grundstuecksflaeche,
       mopube_grundstueck.geom
  FROM be.mopube_grundstueck, be.uzp_bauzonen
  WHERE bfsnr = 360 
  AND ST_Intersects(mopube_grundstueck.geom,uzp_bauzonen.geom) 
  AND bfs = 360 
  AND zone_lo NOT IN ('Landwirtschaftszone / ungezontes Gebiet')
  AND ST_Within(mopube_grundstueck.geom,ST_SetSRID(ST_GeomFromText('Polygon ((2594707.0389655982144177 1204297.62532898178324103, 
2595275.660759671125561 1203316.75273420615121722, 2595881.24297035858035088 1203410.57533022807911038, 
2596688.68591794185340405 1203910.9625090123154223, 2596103.00547004723921418 1204871.93334099510684609, 
2596103.00547004723921418 1204871.93334099510684609, 2595568.50098361866548657 1204749.67965526948682964, 
2594707.0389655982144177 1204297.62532898178324103))'),2056)) 
  AND nummer NOT IN ('6199','4251','4907','5825') 
) AS t
WHERE anrechenbare_grundstuecksflaeche>1
GROUP BY pid, nummer, zone_code
ORDER BY 2;

--Remap Zones
UPDATE dencity.parzelle
SET
zone_code=
    CASE 
      WHEN zone_code = 'Wohnzone 1-geschossig' THEN 'Z1'
      WHEN zone_code = 'Dorfzone, 2 geschossig' THEN 'Z2'
      WHEN zone_code = 'Wohnzone 2-geschossig' THEN 'Z2'
      WHEN zone_code = 'Wohnzone 3-geschossig' THEN 'Z3'
      WHEN zone_code = 'Mischzone' THEN 'Z4'
      WHEN zone_code = 'Mischzone Landwirtschaft' THEN 'Z4'
      WHEN zone_code = 'Zone für öffentliche Nutzung' THEN 'Z5'
      ELSE 'Z5'
    END
WHERE pid = 3;



