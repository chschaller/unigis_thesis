--
--Insert test data
--
DELETE FROM public.scenario;
DELETE FROM public.project;

INSERT INTO public.project(id, title)
    VALUES (1, 'Dotzigen');
INSERT INTO public.project(id, title)
    VALUES (2, 'Bern: Belvedère Länggasse');
INSERT INTO public.project(id, title)
    VALUES (3, 'Wohlen: Uetligen');
INSERT INTO public.project(id, title)
    VALUES (4, 'Langenthal');
INSERT INTO public.project(id, title)
    VALUES (5, 'Steffisburg');    

INSERT INTO public.scenario(title, pid)
    VALUES ('Basis', 1);
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis W3', 1);    
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis', 2);
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis W3', 2);    
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis W4', 2);    
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis', 3);
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis', 4);
INSERT INTO public.scenario(title, pid)
    VALUES ('Szenario 2', 4);
INSERT INTO public.scenario(title, pid)
    VALUES ('Basis', 5);
INSERT INTO public.scenario(title, pid)
    VALUES ('Szenario 2', 5);

    
    
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Setback_Major', '5.0');
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Setback_Minor', '2.091');
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Storey_Count', '2');
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Storey_Height', '3.5');
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Utilization_Factor', '0.5');
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Utilization_Type', '1');
INSERT INTO public.scenario_parameter_generic
    (sid, zone_code, parameter_name, parameter_value)
    VALUES (1, 'z1', 'Zone_Parameters', '{ "groups": [{"type": "row", "parcel_numbers": ["221", "754", "755"]},
{"type": "row", "parcel_numbers": ["650","649","648","647"]},
{"type": "row", "parcel_numbers": ["645","640","639","638"]},
{"type": "block", "parcel_numbers": ["217","28","237","258","382","357","356","134","307"]},
{"type": "block", "parcel_numbers": ["142","766","765","763","128","121","393","130","569"]}
]}');


    





