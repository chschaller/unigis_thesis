DO $$
DECLARE  
  i integer := 0;

  ----Upper bound
  --ausnutzungArten integer[] := ARRAY[0,1,2,3];
  --ausnutzungen real[] := ARRAY[0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0];
  --gruenflaechenziffern real[] := ARRAY[0.0,0.25,0.5,0.75,1.0];
  --kleineGrenzabstaende integer[] := ARRAY[2,4,6];
  --grosseGrenzabstaende integer[] := ARRAY[2,4,6,8,10];
  --geschosszahlen integer[] := ARRAY[1,2,3,4,5];
  --geschosshoehen real[] := ARRAY[2.8,3.0,3.4];
  --zonen varchar[] := ARRAY['Z1','Z2','Z3','Z4','Z5'];

  --Sensible/Animation
  ausnutzungArten integer[] := ARRAY[1,2,3];
  ausnutzungen real[] := ARRAY[0.4,0.6,0.8,1.0];
  gruenflaechenziffern real[] := ARRAY[0.0,0.5];
  kleineGrenzabstaende integer[] := ARRAY[2,4];
  grosseGrenzabstaende integer[] := ARRAY[6,8,10];
  geschosszahlen integer[] := ARRAY[2,3,4];
  geschosshoehen real[] := ARRAY[2.8];
  zonen varchar[] := ARRAY['Z1','Z2','Z3','Z4','Z5'];

  ausnutzungArt integer := 0;
  ausnutzung real := 0.2;
  gruenflaechenziffer real := 0.0;
  kleinerGrenzabstand integer := 2;
  grosserGrenzabstand integer := 2;
  gebaeudehoehe real := 10;
  geschosszahl integer := 1;
  geschosshoehe real := 3.0;

  --1;"Dotzigen", 2;"Bern: Belvedère Länggasse", 4;"Langenthal", 5;"Steffisburg", 3;"Wohlen: Uettligen"
  projekt integer := 2;
  szenario integer := 2;
  szenarioTitel varchar(30) := '';
  szenarioBeschreibung varchar(512) := '';
  ausnutzungArtLabel varchar(30) := '';
  zone varchar := '';
  v_nk RECORD;
BEGIN  

  DELETE FROM dencity.szenario_parameter WHERE sid IN (SELECT id FROM dencity.szenario WHERE pid = projekt AND titel LIKE 'g_%');

  DELETE FROM dencity.szenario WHERE pid = projekt AND titel LIKE 'g_%';

  FOREACH ausnutzungArt IN ARRAY ausnutzungArten
  LOOP
    FOREACH kleinerGrenzabstand IN ARRAY kleineGrenzabstaende
    LOOP
      FOREACH grosserGrenzabstand IN ARRAY grosseGrenzabstaende
      LOOP
        IF grosserGrenzabstand>=kleinerGrenzabstand THEN
          FOREACH geschosszahl IN ARRAY geschosszahlen
          LOOP
            gebaeudehoehe := 3*(geschosszahl+1.5);
            FOREACH geschosshoehe IN ARRAY geschosshoehen
            LOOP
              FOREACH ausnutzung IN ARRAY ausnutzungen
              LOOP
                IF ausnutzungArt<>3 OR (ausnutzungArt=3 AND ausnutzung<=1.0) THEN
                  FOREACH gruenflaechenziffer IN ARRAY gruenflaechenziffern
                  LOOP
                    i := i+1;
                    --Keine = 0, GFZ   = 1, BMZ   = 2, ÜZ    = 3
                    ausnutzungArtLabel := CASE WHEN ausnutzungArt = 1 THEN 'GFZ' WHEN ausnutzungArt = 2 THEN 'BMZ' WHEN ausnutzungArt = 3 THEN 'ÜZ' ELSE 'K' END;  
                    szenarioTitel := 'g_' || ausnutzungArtLabel || '_' || ausnutzung || '_' || grosserGrenzabstand || '_' || kleinerGrenzabstand || '_' || geschosszahl || '_' ||  geschosshoehe || '_' || gebaeudehoehe || '_' || gruenflaechenziffer;
                    szenarioBeschreibung := 'Ausnützung Art:' || ausnutzungArtLabel || '; Ausnutzung: ' || ausnutzung || '; Grosser Grenzabstand:' || grosserGrenzabstand || '; Kleiner Grenzabstand:' || kleinerGrenzabstand || '; Geschosszahl:' || geschosszahl || '; Geschosshöhe:' || geschosshoehe || '; Maximale Gebäudehöhe:' || gebaeudehoehe || '; Grünflächenziffer:' || gruenflaechenziffer;
                  
                    INSERT INTO dencity.szenario(titel, beschreibung, pid) VALUES (szenarioTitel, szenarioTitel, projekt) RETURNING id INTO szenario;
                  
                    FOREACH zone IN ARRAY zonen
                    LOOP
                      --"Ausnuetzung_Art"
                      --"Kleiner_Grenzabstand"
                      --"Grosser_Grenzabstand"
                      --"Zonenparameter"
                      --"Gruenflaechenziffer"
                      --"Gesamthoehe"
                      --"Anteil_Arbeitsnutzung"
                      --"Platzbedarf_Arbeitsnutzung"
                      --"Platzbedarf_Wohnnutzung"
                      --"Nutzungsziffer"
                      --"Geschosshoehe"
                      --"Geschosszahl"
                      
                      --Parametrized
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Ausnuetzung_Art', CAST(ausnutzungArt AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Kleiner_Grenzabstand', CAST(kleinerGrenzabstand AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Grosser_Grenzabstand', CAST(grosserGrenzabstand AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Geschosshoehe', CAST(geschosshoehe AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Geschosszahl', CAST(geschosszahl AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Nutzungsziffer', CAST(ausnutzung AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Gruenflaechenziffer', CAST(gruenflaechenziffer AS VARCHAR(1024)));
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Gesamthoehe', CAST(gebaeudehoehe AS VARCHAR(1024)));
                      
                      --Fixed
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Anteil_Arbeitsnutzung', '0');
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Platzbedarf_Arbeitsnutzung', '20');
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Platzbedarf_Arbeitsnutzung', '60');
                      INSERT INTO dencity.szenario_parameter(sid, zone_code, parameter_name, parameter_wert) VALUES (szenario, zone, 'Zonenparameter', '{}');
                  
                    END LOOP;                      
                  END LOOP; 
                END IF;                  
              END LOOP;
            END LOOP;
          END LOOP;
        END IF;
      END LOOP;
    END LOOP;
  END LOOP;
  
  
  RAISE NOTICE 'Anzahl Szenarien %', i;
  RAISE NOTICE 'Szenario %', szenarioTitel;
  
END $$;
    