--
--Function for updating timestamp column 
--Basing on https://www.revsys.com/blog/2006/aug/04/automatically-updating-a-timestamp-column-in-postgresql/
--
CREATE OR REPLACE FUNCTION update_modified_column()   
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified = now();
    RETURN NEW;   
END;
$$ language 'plpgsql';

--Drop Scenario tables

DROP TABLE IF EXISTS public.building;
DROP TABLE IF EXISTS public.parcel;
DROP TABLE IF EXISTS public.limit_line;
DROP TABLE IF EXISTS public.scenario_parameter;
DROP TABLE IF EXISTS public.scenario_parameter_generic;
DROP TABLE IF EXISTS public.scenario;
DROP TABLE IF EXISTS public.project;

--Create Scenario tables

--
--Project table
--
CREATE TABLE public.project
(
  id serial NOT NULL,
  title character varying(30),
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT project_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.project
  OWNER TO postgres;

CREATE TRIGGER update_project_modtime BEFORE UPDATE ON project FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

--
--Base Scenario table
--
CREATE TABLE public.scenario
(
  id serial NOT NULL,
  title character varying(30),
  pid int NOT NULL REFERENCES project (id), 
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT scenario_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.scenario
  OWNER TO postgres;

CREATE TRIGGER update_scenario_modtime BEFORE UPDATE ON scenario FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

--
--Scenario parameters table
--
CREATE TABLE public.scenario_parameter
(
  id serial NOT NULL,
  sid int NOT NULL REFERENCES scenario (id), 
  zone_code character varying(100),
  setback_major real DEFAULT 0.0 NOT NULL,
  setback_minor real DEFAULT 0.0 NOT NULL,
  storey_count int DEFAULT 1 NOT NULL,
  storey_height real DEFAULT 0.0 NOT NULL,
  utilization_factor real DEFAULT 0.0 NOT NULL,
  zone_parameters character varying(1024),
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT scenario_parameter_unique UNIQUE (sid, zone_code),
  CONSTRAINT scenario_parameter_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.scenario_parameter
  OWNER TO postgres;

CREATE TRIGGER update_scenario_parameter_modtime BEFORE UPDATE ON scenario_parameter FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();


--
--Scenario parameters table
--
CREATE TABLE public.scenario_parameter_generic
(
  id serial NOT NULL,
  sid int NOT NULL REFERENCES scenario (id), 
  zone_code character varying(100),
  parameter_name character varying(100),
  parameter_value character varying(1024),
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT scenario_parameter_generic_unique UNIQUE (sid, zone_code, parameter_name),
  CONSTRAINT scenario_parameter_generic_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.scenario_parameter_generic
  OWNER TO postgres;

CREATE TRIGGER update_scenario_parameter_generic_modtime BEFORE UPDATE ON scenario_parameter_generic FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();



--
--Parcel tables
--
CREATE TABLE public.parcel
(
  gid serial NOT NULL,
  pid int NOT NULL REFERENCES project (id), 
  parcel_number character varying(15),
  zone_code character varying(100),
  geom geometry(MultiPolygon,2056),
  CONSTRAINT parcel_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.parcel
  OWNER TO postgres;

CREATE INDEX parcel_geom_idx
  ON public.parcel
  USING gist
  (geom);
  
SELECT Populate_Geometry_Columns('public.parcel'::regclass);  

--
--Limit line table
--
CREATE TABLE public.limit_line
(
  gid serial NOT NULL,
  sid int NOT NULL REFERENCES scenario (id), 
  distance real DEFAULT 0.0 NOT NULL,
  constitutive boolean DEFAULT false NOT NULL,  
  geom geometry(Linestring,2056),
  CONSTRAINT limit_line_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.limit_line
  OWNER TO postgres;

CREATE INDEX limit_line_geom_idx
  ON public.parcel
  USING gist
  (geom);

SELECT Populate_Geometry_Columns('limit_line'::regclass);
  
--
--Buildings tables
--
CREATE TABLE public.building
(
  gid serial NOT NULL,
  sid int NOT NULL REFERENCES scenario (id), 
  height real DEFAULT 0.0 NOT NULL,
  storey_count int DEFAULT 1 NOT NULL,
  geom geometry(MultiPolygon,2056),
  modified timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT builing_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.building
  OWNER TO postgres;

CREATE INDEX building_geom_idx
  ON public.building
  USING gist
  (geom);  
  
CREATE TRIGGER update_building_modtime BEFORE UPDATE ON building FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

SELECT Populate_Geometry_Columns('public.building'::regclass);