
--Global
SELECT kg.sid
    ,szenario.titel
    ,CASE WHEN szenario.pid = 4 THEN 'Langenthal' ELSE 'Uettligen' END AS Projekt
    ,szenario.pid
    ,kg.anrechenbare_grundstuecksflaeche
    ,kg.area_parzelle
    ,kg.area_gebaeude
    ,kg.volumen
    ,kg.geschossflaeche
    ,kg.bmz 
    ,kg.gfz
    ,kg.uz
    ,ki.avgUZ
    ,ki.avgGFZ
    ,ki.avgBMZ
    ,ki.minUZ
    ,ki.minGFZ
    ,ki.minBMZ
    ,ki.maxUZ
    ,ki.maxGFZ
    ,ki.maxBMZ
    ,pv.Grosser_Grenzabstand
    ,pv.Kleiner_Grenzabstand
    ,pv.Geschosszahl    
    ,pv.Geschosshoehe
    ,pv.Nutzungsziffer    
    ,pv.Ausnuetzung_Art    
    ,pv.Ausnuetzung_Art_Label    
    ,pv.Gruenflaechenziffer    
    ,pv.Gesamthoehe    
    ,pv.Anteil_Arbeitsnutzung
    ,pv.Anteil_Wohnnutzung    
    ,pv.Platzbedarf_Arbeitsnutzung    
    ,pv.Platzbedarf_Wohnnutzung
	,CASE WHEN Zonenparameter='{}' THEN false ELSE true END AS Gruppierung
FROM
(SELECT 
    szenario.id AS sid
    ,anrechenbare_grundstuecksflaeche
    ,area_parzelle
    ,area_gebaeude
    ,geschossflaeche
    ,volumen
    ,COALESCE(volumen,0)/anrechenbare_grundstuecksflaeche AS bmz
    ,COALESCE(geschossflaeche,0)/anrechenbare_grundstuecksflaeche AS gfz
    ,COALESCE(area_gebaeude,0)/anrechenbare_grundstuecksflaeche AS uz
  FROM dencity.szenario
  LEFT JOIN 
    (SELECT
         pid
         ,SUM(parzelle.anrechenbare_grundstuecksflaeche) AS anrechenbare_grundstuecksflaeche
         ,SUM(ST_Area(parzelle.geom)) AS area_parzelle 
       FROM dencity.parzelle 
       GROUP BY pid) AS pc ON szenario.pid = pc.pid
  LEFT JOIN 
    (SELECT 
         sid
         ,SUM(ST_Area(gebaeude.geom)) AS area_gebaeude
         ,SUM(gebaeude.geschossflaeche) AS geschossflaeche
         ,SUM(ST_Area(gebaeude.geom)*gebaeude.hoehe) AS volumen
       FROM dencity.gebaeude
	   WHERE sid BETWEEN 1462 AND 5781
       GROUP BY sid) AS gb ON gb.sid = szenario.id
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 1462 AND 5781)
  AS kg
LEFT JOIN   
--Individual
(SELECT 
sid,
AVG(uz) AS avgUZ,
AVG(gfz) AS avgGFZ,
AVG(bmz) AS avgBMZ,
MIN(uz) AS minUZ,
MIN(gfz) AS minGFZ,
MIN(bmz) AS minBMZ,
MAX(uz) AS maxUZ,
MAX(gfz) AS maxGFZ,
MAX(bmz) AS maxBMZ
FROM
(SELECT sid, pgid,
    SUM(gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS uz,
    SUM(geschosszahl*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS gfz,
    SUM(hoehe*gebaeude_area)/MIN(anrechenbare_grundstuecksflaeche) AS bmz
FROM
(
SELECT 
    szenario.id AS sid,
    parzelle.gid AS pgid,
    parzelle.nummer,
    parzelle.anrechenbare_grundstuecksflaeche,
    ST_Area(parzelle.geom) AS parzelle_area,
    COALESCE(gebaeude.hoehe,0) AS hoehe,
    COALESCE(gebaeude.geschosszahl,0) AS geschosszahl,
    gebaeude.geschossflaeche,
    gebaeude.nutzer_wohnen,
    gebaeude.nutzer_arbeit,
    COALESCE(ST_Area(ST_Intersection(gebaeude.geom,parzelle.geom)),0) gebaeude_area,
    gebaeude.anteil_arbeitsnutzung
  FROM dencity.szenario
  LEFT JOIN dencity.parzelle ON szenario.pid = parzelle.pid
  LEFT JOIN dencity.gebaeude ON gebaeude.sid = szenario.id AND ST_Intersects(parzelle.geom, gebaeude.geom)
  WHERE szenario.pid IN (4,3) AND szenario.id BETWEEN 1462 AND 5781
) AS i1
GROUP BY sid, pgid
) AS i2
GROUP BY sid) AS ki ON kg.sid = ki.sid
LEFT JOIN
--Parameter
  (SELECT sid, 
      MAX(Grosser_Grenzabstand) AS Grosser_Grenzabstand,
      MAX(Kleiner_Grenzabstand) AS Kleiner_Grenzabstand,
      MAX(Geschosszahl) AS Geschosszahl,
      MAX(Geschosshoehe) AS Geschosshoehe,
      MAX(Nutzungsziffer) AS Nutzungsziffer,
      MAX(Ausnuetzung_Art) AS Ausnuetzung_Art,
      MAX(Ausnuetzung_Art_Label) AS Ausnuetzung_Art_Label,
      MAX(Zonenparameter) AS Zonenparameter,
      MAX(Gruenflaechenziffer) AS Gruenflaechenziffer,
      MAX(Gesamthoehe) AS Gesamthoehe,
      MAX(Anteil_Arbeitsnutzung) AS Anteil_Arbeitsnutzung,
      MAX(Anteil_Wohnnutzung) AS Anteil_Wohnnutzung,              
      MAX(Platzbedarf_Arbeitsnutzung) AS Platzbedarf_Arbeitsnutzung,
      MAX(Platzbedarf_Wohnnutzung) AS Platzbedarf_Wohnnutzung
  
    FROM 
    (
      SELECT 
          sid
          ,CASE WHEN parameter_name = 'Grosser_Grenzabstand' THEN CAST(parameter_wert AS real) ELSE NULL END AS Grosser_Grenzabstand
          ,CASE WHEN parameter_name = 'Kleiner_Grenzabstand' THEN CAST(parameter_wert AS real) ELSE NULL END AS Kleiner_Grenzabstand
          ,CASE WHEN parameter_name = 'Geschosszahl' THEN CAST(parameter_wert AS real) ELSE NULL END AS Geschosszahl
          ,CASE WHEN parameter_name = 'Geschosshoehe' THEN CAST(parameter_wert AS real) ELSE NULL END AS Geschosshoehe
          ,CASE WHEN parameter_name = 'Nutzungsziffer' THEN CAST(parameter_wert AS real) ELSE NULL END AS Nutzungsziffer
          ,CASE WHEN parameter_name = 'Ausnuetzung_Art' THEN CAST(parameter_wert AS real) ELSE NULL END AS Ausnuetzung_Art
          ,CASE WHEN parameter_name = 'Ausnuetzung_Art' THEN (
              CASE WHEN parameter_wert = '0' THEN 'Keine'
                WHEN parameter_wert = '1' THEN 'GFZ'
                WHEN parameter_wert = '2' THEN 'BMZ'
                WHEN parameter_wert = '3' THEN 'ÜZ'
                ELSE ''
              END
              ) ELSE NULL END AS Ausnuetzung_Art_Label
          ,CASE WHEN parameter_name = 'Zonenparameter' THEN parameter_wert ELSE NULL END AS Zonenparameter
                            
          ,CASE WHEN parameter_name = 'Gruenflaechenziffer' THEN CAST(parameter_wert AS real) ELSE NULL END AS Gruenflaechenziffer
          ,CASE WHEN parameter_name = 'Gesamthoehe' THEN CAST(parameter_wert AS real) ELSE NULL END AS Gesamthoehe
          ,CASE WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Anteil_Arbeitsnutzung
          ,CASE WHEN parameter_name = 'Anteil_Arbeitsnutzung' THEN 1-CAST(parameter_wert AS real) ELSE NULL END AS Anteil_Wohnnutzung
          ,CASE WHEN parameter_name = 'Platzbedarf_Arbeitsnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Platzbedarf_Arbeitsnutzung
          ,CASE WHEN parameter_name = 'Platzbedarf_Wohnnutzung' THEN CAST(parameter_wert AS real) ELSE NULL END AS Platzbedarf_Wohnnutzung
        FROM dencity.szenario_parameter
        WHERE zone_code = 'Z1' AND szenario_parameter.sid BETWEEN 1462 AND 5781
     ) AS p
     GROUP BY sid
  ) AS pv ON pv.sid = kg.sid
  LEFT JOIN dencity.szenario ON kg.sid = szenario.id
  











